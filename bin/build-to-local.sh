#!/bin/bash

TARGET_DIR=/mnt/local/home/rcxue/src/rcxue/osqa/forum/skins/fresh/media/dist

# compile by npm
rm -rf dist
./node_modules/.bin/webpack --config webpack/prod.config.js

rm -rf $TARGET_DIR
mv dist/index.html /mnt/local/home/rcxue/src/rcxue/osqa/forum/skins/fresh/templates/
mv dist/ $TARGET_DIR
