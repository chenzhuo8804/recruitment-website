#!/bin/bash

TARGET_DIR=/mnt/local/home/rcxue/src/rcxue/osqa/forum/skins/fresh/media/dist

# compile by npm
rm -rf dist/*
./node_modules/.bin/webpack --config webpack/prod.config.js

cd /mnt/local/home/rcxue/src/rcxue/osqa/forum/skins/fresh/
git checkout testing
git pull origin testing

cd -

rm -rf $TARGET_DIR
mv dist/index.html /mnt/local/home/rcxue/src/rcxue/osqa/forum/skins/fresh/templates/
mv dist/ $TARGET_DIR

cd /mnt/local/home/rcxue/src/rcxue/osqa/forum/skins/fresh/
git add /mnt/local/home/rcxue/src/rcxue/osqa/forum/skins/fresh/
git commit -m'react rebuild'
echo ===========
git status
echo ===========
git push -f origin testing:react-rebuild
