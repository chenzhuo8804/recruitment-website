# hello, APIs

## Feedback Page

1. 先调用 API-Feedback-001 Foretell 获取表单的 default value
2. 调用 API-Feedback-002 CreateFeedBack 创建新表单


#### API-Feedback-001 Foretell

get default values for the feedback form.


Request:

```http
GET /api/v2/feedback/foretell HTTP/1.1
Authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJ1aS5nb25nQHNlZWRsaW5rdGVjaC5jb20iLCJ1c2VyX2lkIjoyLCJlbWFpbCI6InJ1aS5nb25nQDEuY28iLCJleHAiOjE0NzY5NDIxMTd9.LUO16WfAo7avAZrSX-Gf6g5e-_iiTIHcCMVX3_c5lvE
Host: test.rcxue.com
Connection: close
User-Agent: Paw/3.0.12 (Macintosh; OS X/10.11.5) GCDHTTPRequest
```

Response:

```http
HTTP/1.1 200 OK
Server: nginx/1.10.1
Date: Thu, 20 Oct 2016 04:42:10 GMT
Content-Type: application/json
Transfer-Encoding: chunked
Connection: close
Vary: Accept, Accept-Language, Cookie
ETag: "6d75b96789f534bd8a5ef74fdda1f5b7"
Content-Language: en
Allow: OPTIONS, GET

{
  "name": "Rcxue",
  "position_id": "1001",
  "phone": "13812345678",
  "contact": "email@rcxue.com",
  "mobile": "email@rcxue.com",
  "faq_type": [
    {
      "id": 1,
      "name": "Registration Problems"
    },
    {
      "id": 2,
      "name": "Uploading profile picture or CV failed"
    }
  ]
}
```

#### API-Feedback-002 CreateFeedBack

Request:

```http
POST /api/v2/feedback/ HTTP/1.1
Referer: paw
Authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJ1aS5nb25nQHNlZWRsaW5rdGVjaC5jb20iLCJ1c2VyX2lkIjoyLCJlbWFpbCI6InJ1aS5nb25nQDEuY28iLCJleHAiOjE0NzY5NDIxMTd9.LUO16WfAo7avAZrSX-Gf6g5e-_iiTIHcCMVX3_c5lvE
Content-Type: application/json; charset=utf-8
Host: test.rcxue.com
Connection: close
User-Agent: Paw/3.0.12 (Macintosh; OS X/10.11.5) GCDHTTPRequest
Content-Length: 23

{"description":"hello"}
```

Response:

```http
HTTP/1.1 201 CREATED
Server: nginx/1.10.1
Date: Thu, 20 Oct 2016 04:43:38 GMT
Content-Type: application/json
Transfer-Encoding: chunked
Connection: close
Vary: Accept, Accept-Language, Cookie
ETag: "eae22ab3a9f175c10b4402a69875583a"
Content-Language: en
Allow: POST, OPTIONS

{
  "id": 44,
  "name": "Rcxue",
  "contact": "rcxue@seedlinktech.com",
  "description": "hello",
  "referer": "paw",
  "ua": "Paw/3.0.12 (Macintosh; OS X/10.11.5) GCDHTTPRequest",
  "create_time": "2016-10-20T12:43:38.169587",
  "user": 2
}
```
