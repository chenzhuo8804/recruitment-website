/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackBrowserPlugin = require('webpack-browser-plugin');
const autoprefixer = require('autoprefixer');
// const os = require('os');
//
// const ifaces = os.networkInterfaces();
// let localIP = '127.0.0.1';
// Object.keys(ifaces).forEach((ifname) => {
//     if (ifname.indexOf('en') === 0) {
//         ifaces[ifname].forEach((iface) => {
//             if (iface.family === 'IPv4' && iface.internal === false) {
//                 localIP = iface.address;
//             }
//         });
//     }
// });
const localIP = '127.0.0.1';
const host = (process.env.HOST || localIP);
const port = (process.env.PORT || 6060);
const protocol = process.env.PROTOCOL;
const apiHost = process.env.API_HOST || localIP;
const apiPort = process.env.API_PORT;
const apiProxy = { host: apiHost, protocol, port: +apiPort };
module.exports = {
    devtool: 'inline-source-map',
    entry: {
        seedlink: './app/recruiter/index.js',
    },

    output: {
        path: 'dist',
        filename: '[name].js',
        publicPath: `${protocol}//${host}:${port}/`,
    },

    devServer: {
        historyApiFallback: true,
        host: '0.0.0.0',
        port,
        https: protocol === 'https:',
        proxy: {
            '/api': {
                target: apiProxy,
                // changeOrigin: true,
                secure: false,
            },
            '/open': {
                target: apiProxy,
                // changeOrigin: true,
                secure: false,
            },
            '/admin': {
                target: apiProxy,
                // changeOrigin: true,
                secure: false,
            },
            '/static/media/grappelli/': {
                target: apiProxy,
                // changeOrigin: true,
                secure: false,
            },
        },
    },

    plugins: [
        new HtmlWebpackPlugin({
            inject: false,
            template: 'bolt-html-template.ejs',
            mobile: true,
            appMountId: 'root',
            title: 'Seedlink AI',
        }),
        new WebpackBrowserPlugin({
            browser: 'Chrome',
            port,
            url: `${protocol}//${localIP}`,
        }),
    ],

    module: {
        preLoaders: [
            // { test: /\.js[x]?$/, exclude: /node_modules/, loader: 'eslint' },
        ],
        loaders: [
            { test: /\.scss$/, loader: 'style!css?sourceMap!postcss!sass?outputStyle=expanded&sourceMap' },
            { test: /\.css$/, loader: 'style!css' },
            { test: /\.js[x]?$/, exclude: /node_modules/, loader: 'babel' },
            { test: /\.json$/, loader: 'json' },
            { test: /\.(png|jpe?g|gif)$/, loader: 'url?limit=8192&name=images/[name].[hash:10].[ext]' },
            { test: /\.(woff|woff2|ttf|eot|svg)(\?t=[0-9]+)?$/, loader: 'url?limit=8192&name=fonts/[name].[hash:10].[ext]' },
        ],
    },

    progress: true,

    postcss: [autoprefixer],

};
