/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

const packageSettings = require('../package.json');

module.exports = {
    entry: {
        seedlink: './app/recruiter/index.js',
        vendor: Object.keys(packageSettings.dependencies),
    },

    output: {
        path: 'dist',
        publicPath: '/static/media/dist/',
        filename: 'seedlink.[hash].js',
    },

    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js'),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin(),
        new ExtractTextPlugin('seedlink.[hash].css'),
        new HtmlWebpackPlugin({
            inject: false,
            template: 'bolt-html-template.ejs',
            mobile: true,
            appMountId: 'root',
            title: 'Seedlink AI',
        }),
    ],

    module: {
        preLoaders: [
            // { test: /\.js[x]?$/, exclude: /node_modules/, loader: 'eslint' },
        ],
        loaders: [
            { test: /\.scss$/, loader: ExtractTextPlugin.extract('style-loader', 'css!postcss!sass') },
            { test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css') },
            { test: /\.js[x]?$/, exclude: /node_modules/, loader: 'babel' },
            { test: /\.json$/, loader: 'json' },
            { test: /\.(png|jpe?g|gif)$/, loader: 'url?limit=8192&name=images/[name].[hash:20].[ext]' },
            { test: /\.(woff|woff2|ttf|eot|svg)(\?t=[0-9]+)?$/, loader: 'url?limit=8192&name=fonts/[name].[hash:20].[ext]' },
        ],
    },

    progress: true,

    postcss: [autoprefixer],

    // resolve: {
    //     extensions: ['', '.js'],
    // },
};
