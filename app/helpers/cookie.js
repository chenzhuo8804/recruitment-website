export default class Cookie {
    static get (name) {
        let returnValue = '';
        const documentCookie = document && document.cookie;

        if (name && documentCookie) {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i += 1) {
                const cookie = cookies[i].replace(/(^\s*)|(\s*$)/g, '');
                if (cookie.substring(0, name.length + 1) === (`${name}=`)) {
                    returnValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }

        return returnValue;
    }
}
