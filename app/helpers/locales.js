import { addLocaleData } from 'react-intl';

import enLocaleData from 'react-intl/locale-data/en';
import nlLocaleData from 'react-intl/locale-data/nl';
import zhLocaleData from 'react-intl/locale-data/zh';

import enLocaleMessages from '../../locales/en/rcxue-json-en.json';
import nlLocaleMessages from '../../locales/nl-NL/rcxue-json-nl-NL.json';
import zhLocaleMessages from '../../locales/zh-CN/rcxue-json-zh-CN.json';

addLocaleData([...enLocaleData, ...nlLocaleData, ...zhLocaleData]);

/**
 * react intl only support flat messages
 */
const flattenMessages = (nestedMessages, prefix = '') => (
    Object.keys(nestedMessages).reduce((msg, key) => {
        const messages = { ...msg };
        const value = nestedMessages[key];
        const prefixedKey = prefix ? `${prefix}.${key}` : key;

        if (typeof value === 'string') {
            messages[prefixedKey] = value;
        } else {
            Object.assign(messages, flattenMessages(value, prefixedKey));
        }

        return messages;
    }, {})
);

const supportLanguages = [
  { locale: 'en', messages: flattenMessages(enLocaleMessages) },
  { locale: 'nl', messages: flattenMessages(nlLocaleMessages) },
  { locale: 'zh', messages: flattenMessages(zhLocaleMessages) },
];

/**
 * normalize language tag String to standard language Object
 */
const normalizeTag = (tag) => {
    const splitTag = tag.split('-');
    const locale = splitTag[0];
    const region = splitTag[1];

    return { locale, region };
};

/**
 * test if standard language Objects are same language
 */
const testLanguage = (source, target) => (
    source.locale === target.locale
);

/**
 * search tag string is supported, if true return the support locale
 */
const searchSupport = tag => (
    supportLanguages.find(
        target => testLanguage(normalizeTag(tag), target),
    )
);

const preferLanguage =
        window.navigator.languages ||
        navigator.language ||
        navigator.userLanguage;

export default class Locales {
    /**
     * get supported language tag by the order of prefer, default is browswer languages
     */
    static getLocale (prefer = preferLanguage) {
        return [].concat(prefer).find(tag => searchSupport(tag));
    }

    /**
     * get supported translation messages by the order of prefer, default is browser languages
     */
    static getMessages (prefer = preferLanguage) {
        return searchSupport(this.getLocale(prefer)).messages;
    }
}
