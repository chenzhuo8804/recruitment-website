/* eslint import/prefer-default-export: [0] */

import Cookie from './cookie';
import Locales from './locales';

export {
    Cookie,
    Locales,
};
