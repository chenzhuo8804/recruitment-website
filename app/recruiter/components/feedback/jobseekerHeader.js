import React from 'react';
import { FormattedMessage } from 'react-intl'; // <FormattedMessage id="seedlinkScore"/>
// import './jobseekerHeader.scss';


class JobseekerHeader extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            showBurgerPanel: false,
        };
    }
    showPanel () {
        this.setState({ showBurgerPanel: !this.state.showBurgerPanel });
    }
    render () {
        return (
            <div className="command-bar">
                <div className="seedlink-logo">
                    <img src={require('./feedback-jobseekerHeader-logo.png')} />
                    <h2><FormattedMessage id="js.feedback.seedlink_support" /></h2>
                </div>
                <div className="logout-help">
                    <a className="logout" href="/?login=true"><FormattedMessage id="button.logout" /></a>
                    <a className="help iconfont icon-wenhao" href="/feedback/" target="_blank" />
                </div>
                <div className="feedback-hamburger iconfont icon-hanbao" onClick={this.showPanel.bind(this)} />
                { this.state.showBurgerPanel ?
                    <div className="burgerPanel">
                        <ul>
                            <li><a href="/feedback/" target="_blank"><FormattedMessage id="js.feedback.title" /></a></li>
                            <li><a href="/?login=true"><FormattedMessage id="button.logout" /></a></li>
                        </ul>
                    </div> : null
                }

            </div>
        );
    }
}
// JobseekerHeader.propTypes = {
//     onclick: React.PropTypes.function,
// };
// DemoPop.defaultProps = {
//     myScore: 0,
// }
export default JobseekerHeader;
