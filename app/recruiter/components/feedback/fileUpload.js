import React from 'react';
import request from 'superagent';
import ReactDOM from 'react-dom';
import Cookie from '../../../helpers/cookie';
import { FormattedMessage } from 'react-intl'; // <FormattedMessage id="seedlinkScore"/>
// import Qiniu from 'qiniu';
// import './fileUpload.scss';

// console.log(Qiniu);
class FileUpload extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            imgUploading: false,
            uploadFailed: false,
            qiniuError: false,
            fileName: '',
        };
        this.onClick = this.onClick.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
    }
    uploadFile (e) {
        this.setState({ imgUploading: true });
        const inputFile = e.target.value;
        const fileName = inputFile.split(/(\\|\/)/g).pop();
        this.setState({ fileName });
        const formData = new FormData();
        formData.append('file', inputFile);
        const self = this;
        const data = { origin_filename: fileName };
        const csrftoken = Cookie.get('csrftoken');
        request
        .post(self.props.getTokenUrl)
        .set('Accept', 'application/json')
        .set('X-CSRFToken', csrftoken)
        // .set('Authorization', 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFteS5zaGFuZ0BzZWVkbGlua3RlY2guY29tIiwidXNlcl9pZCI6NzU3MTksImVtYWlsIjoiYW15LnNoYW5nQHNlZWRsaW5rdGVjaC5jb20iLCJleHAiOjE0ODA1MDAwNDd9.8t8MN5UMOo5AF-MVmtolY7lOrFGjLCk_TcArZT9BbMU')
        .send(data)
        .end((err, res) => {
            if (err || !res.ok) {
                self.setState({ uploadFailed: true, qiniuError: true, imgUploading: false });
            } else {
                const response = JSON.parse(res.text);
                const tokenKey = response.token;
                const key = response.file_key;
                const uploadHost = response.upload_host;
                self.uploadFileToQiniu(inputFile, tokenKey, key, uploadHost);
            }
        });
        e.preventDefault();
    }
    uploadFileToQiniu (file, token, key, uploadHost) {
        const formData = new FormData();
        formData.append('token', token);
        formData.append('file', file);
        formData.append('key', key);
        const self = this;

        request
            .post(uploadHost)
            .send(formData)
            .end((err, res) => {
                if (err || !res.ok) {
                    self.setState({ uploadFailed: true, qiniuError: true, imgUploading: false });
                } else if (res.status === 200) {
                    const response = JSON.parse(res.text);
                    const fileKey = response.key;
                    self.props.sendFileKey(fileKey);
                    self.setState({ imgUploading: false });
                } else {
                    self.setState({ uploadFailed: true, qiniuError: true, imgUploading: false });
                }
            });
    }
    onClick () {
        this.open();
    }
    open () {
        const fileInput = ReactDOM.findDOMNode(this.refs.fileInput);
        fileInput.value = null;
        fileInput.click();
    }
    render () {
        const buttonDisabled = this.state.imgUploading;
        return (
            <div>
                <input ref="fileInput" id="input-file" type="file" accept={this.props.acceptFileType} name="file" className="upload-file" onChange={this.uploadFile} />
                <button disabled={buttonDisabled} onClick={this.onClick}><FormattedMessage id={this.state.imgUploading ? 'stat_uploading' : 'button.upload'} /></button>
                { this.state.uploadFailed ?
                    <p className="errorMessage"><FormattedMessage id={this.state.qiniuError ? this.props.qiniuFailedHint : this.props.uploadFailedHint} /></p> : <p className="fileName">{this.state.fileName}</p>
                }
            </div>
        );
    }
}
FileUpload.propTypes = {
    getTokenUrl: React.PropTypes.string,
    uploadFailedHint: React.PropTypes.string,
    qiniuFailedHint: React.PropTypes.string,
    acceptFileType: React.PropTypes.string,
    sendFileKey: React.PropTypes.func,
    checkFileUploading: React.PropTypes.func,
};
FileUpload.defaultProps = {
    uploadFailedHint: 'error.submit_failed',
    qiniuFailedHint: 'error.submit_failed_external',
};
export default FileUpload;
