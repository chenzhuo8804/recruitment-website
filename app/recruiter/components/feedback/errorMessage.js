import React from 'react';
import { FormattedMessage } from 'react-intl'; // <FormattedMessage id="seedlinkScore"/>
// import './errorMessage.scss';

class ErrorMessage extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            inputValue: '',
        };
    }
    render () {
        return (
            <p className="ErrorMessage"><FormattedMessage id={this.props.errorContent} /></p>
        );
    }
}
ErrorMessage.propTypes = {
    errorContent: React.PropTypes.string,
};
export default ErrorMessage;
