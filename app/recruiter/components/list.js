import React from 'react';
// import './list.scss';
import ListItem from './listItem';

class List extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            selectedItem: null,
            currentItem: false,
        };
    }
    clickHandler (index, itemID, itemName) {
        this.setState({ selectedItem: index, currentItem: true });
        this.props.getSelectedData(itemID, itemName);
    }

    render () {
        const items = this.props.data.map((item, idx) => {
            const isSelected = this.state.selectedItem === idx;
            return (<ListItem
              key={idx}
              name={item.name}
              onClick={this.clickHandler.bind(this, idx, item.id, item.name)}
              isSelected={isSelected}
                    // selectedColor={this.props.selectedColor}
              curr={isSelected ? 'cur' : null}
            />);
        });
        return (
            <ul>
                {items}
            </ul>
        );
    }
}

List.propTypes = {
    getSelectedData: React.PropTypes.func.isRequired,
    // selectedColor: React.PropTypes.string,
    // hoveredColor: React.PropTypes.string,
    // liBackgroundColor: React.PropTypes.string,
};


export default List;
