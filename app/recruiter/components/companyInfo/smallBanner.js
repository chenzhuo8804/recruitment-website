import React from 'react';
import { FormattedMessage } from 'react-intl';
// import './smallBanner.scss';

class SmallBanner extends React.Component {
    constructor (props) {
        super(props);

        // this.state = {
        // }
    }

    render () {
        return (
            <div className="smallBanner">
                <div className="green-line" />
                <h3><FormattedMessage id={this.props.content} /></h3>
            </div>
        );
    }
}
SmallBanner.propTypes = {
    content: React.PropTypes.string,
};

export default SmallBanner;
