import React, { PropTypes } from 'react';
import classNames from 'classnames';

const propTypes = {
    className: PropTypes.string,
    iconName: PropTypes.string.isRequired,
};

const IconFont = props => (
    <span className={classNames('iconfont', props.iconName, props.className)} />
);

IconFont.propTypes = propTypes;

export default IconFont;
