import React, { Component } from 'react';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';

const propTypes = {
    intl: intlShape.isRequired,
};

class Login extends Component {

    constructor (props) {
        super(props);

        this.state = {
            isFetch: false,
            username: '',
            password: '',
        };

        this.handleUserName = this.handleUserName.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleUserName (event) {
        this.setState({ username: event.target.value });
    }

    handlePassword (event) {
        this.setState({ password: event.target.value });
    }

    handleLogin (event) {

    }

    render () {
        const { formatMessage } = this.props.intl;

        return (
            <div className="home-login">
                <div className="login-dialog">
                    <div className="login-title">
                        <FormattedMessage id="hr.login" />
                    </div>
                    <form>
                        <div className="login-field">
                            <input
                                className="login-input" type="text" value={this.state.username}
                                placeholder={formatMessage({ id: 'hint.login_id' })}
                                onChange={this.handleUserName}
                            />
                        </div>
                        <div className="login-field">
                            <input
                                className="login-input" type="password" value={this.state.password}
                                placeholder={formatMessage({ id: 'hint.login_password' })}
                                onChange={this.handlePassword}
                            />
                        </div>
                        <div className="login-link">
                            <a className="link" href="#">
                                <FormattedMessage id="hr.forgot_password" />
                            </a>
                        </div>
                        <div className="login-action">
                            <button className="button" onClick={this.handleLogin}>
                                <FormattedMessage id="button.login" />
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

Login.propTypes = propTypes;

export default injectIntl(Login);
