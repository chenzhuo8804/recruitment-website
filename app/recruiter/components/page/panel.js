import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/dedupe';

const propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    scrollable: PropTypes.bool,
};

class Panel extends Component {
    constructor (props) {
        super(props);

        this.state = {
            panelOffsetY: 0,
            isScrolling: false,
            scrollTimeout: undefined,
        };

        this.handleWheel = this.handleWheel.bind(this);
    }

    getContainerStyle () {
        const { panelOffsetY: py } = this.state;
        const style = {};

        if (py) {
            style.transform = `translateY(${py}px)`;
        }

        return style;
    }

    getScrollbarHeight () {
        const { offsetHeight: ch } = this.container;
        const { offsetHeight: ph } = this.panel;
        const { clientHeight: psh } = this.panelScroll;

        return Math.min(psh, Math.round((ch / ph) * psh));
    }

    getScrollbarOffset () {
        const { panelOffsetY: py } = this.state;
        const { offsetHeight: ch } = this.container;
        const { offsetHeight: ph } = this.panel;
        const { clientHeight: psh } = this.panelScroll;
        const sh = this.getScrollbarHeight();

        return Math.round((-py / (ph - ch)) * (psh - sh));
    }

    getScrollbarStyle () {
        let style = {};

        if (this.container && this.panel) {
            style = {
                height: `${this.getScrollbarHeight()}px`,
                transform: `translateY(${this.getScrollbarOffset()}px)`,
            };
        }

        return style;
    }

    handleWheel (event) {
        if (!this.props.scrollable) return;

        const { deltaY } = event;
        const { offsetHeight: ch } = this.container;
        const { offsetHeight: ph } = this.panel;

        clearTimeout(this.state.scrollTimeout);

        this.setState(prevState => ({
            isScrolling: true,
            scrollTimeout: setTimeout(() => { this.setState({ isScrolling: false }); }, 1000),
            panelOffsetY: Math.max(Math.min(prevState.panelOffsetY - (deltaY * 4), 0), ch - ph),
        }));

        event.stopPropagation();
    }

    render () {
        const { className, children, scrollable } = this.props;

        return (
            <div
                className={classNames('page-panel', className)}
                onWheel={this.handleWheel}
                ref={(container) => { this.container = container; }}
            >
                <div
                    className="panel-container"
                    ref={(panel) => { this.panel = panel; }}
                    style={this.getContainerStyle()}
                >
                    {children}
                </div>
                { scrollable &&
                    <div
                        className={classNames('panel-scroll', { scrolling: this.state.isScrolling })}
                        ref={(panelScroll) => { this.panelScroll = panelScroll; }}
                    >
                        <div
                            className="scroll-bar"
                            ref={(scrollBar) => { this.scrollBar = scrollBar; }}
                            style={this.getScrollbarStyle()}
                        />
                    </div>
                }
            </div>
        );
    }
}

Panel.propTypes = propTypes;

export default Panel;
