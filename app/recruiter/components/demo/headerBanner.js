import React from 'react';
// import './headerBanner.scss';
import { FormattedMessage } from 'react-intl';

class HeaderBanner extends React.Component {
    render () {
        return (
            <div className="headerBanner">
                <div className="background-img">
                    <div>
                        <FormattedMessage id="demo.title" tagName="h1" />
                        <FormattedMessage id="demo.description" tagName="h2" />
                    </div>
                </div>
            </div>
        );
    }
}

export default HeaderBanner;
