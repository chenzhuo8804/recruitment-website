import React from 'react';
import Request from 'superagent';
// import './demoSubmit.scss';
import { FormattedMessage } from 'react-intl';
import List from '../list';


class DemoSubmit extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            startValidation: false,
            typeId: '',
            workYearId: '',
            length: 0,
            language: this.getLanguage(),
            emclassName: '',
            inputText: '',
            inputLang: '',
            browserLang: '',
            invalidInputLang: false,
            checkingLangStatus: false,
        };
        this.getJobTypes = this.getJobTypes.bind(this);
        this.getExpLevels = this.getExpLevels.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.wordCounting = this.wordCounting.bind(this);
    }
    getLanguage () {
        const language = navigator.languages ? navigator.languages[0] : navigator.language || navigator.userLanguage;
        const generalLang = language.substring(0, 2);
        let useLang;
        if (generalLang === 'zh') {
            useLang = 'zh';
        } else {
            useLang = 'en';
        }
        return useLang;
    }

    wordCounting (event) {
        const input = event.target.value.trim();
        let total = 0;
        if (this.state.language === 'zh') {
            total = this.checkChineseChar(input);
        } else {
            total = input.split(' ').length;
        }
        total = Math.ceil(total);
        if (total >= 100) {
            this.setState({
                emclassName: 'wordEnough',
            });
        } else {
            this.setState({
                emclassName: '',
            });
        }
        this.setState({
            length: total,
            inputText: event.target.value,
        });
    }

    getJobTypes (itemID, itemName) {
        this.setState({ typeId: itemID });
    }
    getExpLevels (itemID, itemName) {
        this.setState({ workYearId: itemID });
    }
    handleSubmit () {
        this.setState({
            startValidation: true,
        });
        this.validateInput();
    }
    validateInput () {
        const workType = this.state.typeId;
        const expLevel = this.state.workYearId;
        const inputText = this.state.inputText;
        // const totalWords = this.state.length;
        const firstHundred = inputText.slice(0, 100);
        if (workType !== '' && expLevel !== '' && inputText !== '') {
            this.checkInputLang(firstHundred);
        }
    }
    checkInputLang (answer) {
        const self = this;
        self.setState({
            checkingLangStatus: true,
        });
        Request
        .get(`/api/v2/multilingual/classify/?text=${answer}`)
        .end((err, res) => {
            if (err || !res.ok) {
                self.setState({
                    checkingLangStatus: false,
                });
            } else if (res.status === 200) {
                self.setState({
                    checkingLangStatus: false,
                });
                const responseJson = JSON.parse(res.text);
                self.setState({ inputLang: responseJson.language });
                if (self.state.language !== self.state.inputLang) {
                    self.setState({
                        invalidInputLang: true,
                    });
                } else {
                    self.setState({
                        invalidInputLang: false,
                    });
                    if (self.state.length > 100) {
                        this.submitResult();
                    }
                }
            }
        });
    }

    submitResult () {
        const postSelectData = {};
        postSelectData.type = this.state.typeId;
        postSelectData.work_year = this.state.workYearId;
        postSelectData.language = this.state.language;
        postSelectData.answer = this.state.inputText;
        this.props.callbackParent(postSelectData);
    }
    checkChineseChar (string) {
        let s = 0;
        for (let i = 0; i < string.length; i++) {
            if (string.charAt(i).match(/[u0391-uFFE5]/)) {
                s += 2;
            } else {
                s++;
            }
        }
        s = s + 1;
        return s;
    }

    render () {
        return (
            <div className="hr-main demoSubmit">
                <div className="RadioBox" id="RadioBox">
                    <div className="selectInfor">
                        <p><FormattedMessage id="question.job_category" /></p>
                        <List data={this.props.jobTypes} getSelectedData={this.getJobTypes} />
                        { this.state.startValidation && this.state.typeId === '' ?
                            <span className="errorMessage"><FormattedMessage id="hint_select_category" /></span> : null
                        }
                    </div>
                    <div className="selectInfor">
                        <p><FormattedMessage id="question.work_experience" /></p>
                        <List data={this.props.expLevels} getSelectedData={this.getExpLevels} />
                        { this.state.startValidation && this.state.workYearId === '' ?
                            <span className="errorMessage"><FormattedMessage id="hint_select_exp" /></span> : null
                        }
                    </div>
                </div>
                <div className="wordCounting">
                    <p className="demoQues"><FormattedMessage id="demo.question" /></p>
                    <div className="textareaContainer">
                        <textarea value={this.state.inputText} onChange={this.wordCounting} />
                        {this.state.checkingLangStatus ?
                            <div className="checkingLangPop"><FormattedMessage id="stat_checking_language" /></div>
                            :
                            null
                        }
                    </div>
                    <div className="wordTips">
                        { this.state.invalidInputLang ?
                            <p className="answerLanguage">
                                <FormattedMessage id="interview.language_tip" />
                            </p>
                            :
                            null
                        }
                        <p className="atLest">
                            <FormattedMessage
                              id="questions.wording_count_tip"
                              tagName="span"
                              values={{
                                  word_count: <em className={this.state.emclassName} > {this.state.length} / 100 </em>,
                              }}
                            />
                        </p>
                    </div>
                </div>

                <button disabled={this.state.checkingLangStatus} onClick={this.handleSubmit}>
                    <FormattedMessage id="button.submit" />
                </button>
            </div>
        );
    }
}
DemoSubmit.propTypes = {
    callbackParent: React.PropTypes.func,
};
export default DemoSubmit;
