import React from 'react';
import { FormattedMessage } from 'react-intl';
// import './slscorePop.scss';

class SlscorePop extends React.Component {
    render () {
        return (
            <div className="slscore-context">
                <p className="score-title"><FormattedMessage id="seedlinkscore.0" /></p>
                <p><FormattedMessage id="seedlinkscore.description" /></p>
                <div className="cards-container">
                    <div className="cards basic">
                        <p className="card-title"><FormattedMessage id="seedlinkscore.volumn_full" /></p>
                        <div className="items">
                            <span className="item"><FormattedMessage id="seedlinkscore.description_volumn" /></span>
                        </div>
                        <div className="why-choose">
                            <p><FormattedMessage id="seedlinkscore.why" /> <span><FormattedMessage id="seedlinkscore.volumn" /></span> ?</p>
                            <p className="content"><FormattedMessage id="seedlinkscore.why_volumn" /></p>
                        </div>

                    </div>
                    <div className="cards pro">
                        <p className="card-title"><FormattedMessage id="seedlinkscore.pro_full" /></p>
                        <div className="items">
                            <span className="item"><FormattedMessage id="seedlinkscore.volumn_full" /></span>
                            <span className="item"><FormattedMessage id="seedlinkscore.description_pro" /></span>
                        </div>
                        <div className="why-choose">
                            <p><FormattedMessage id="seedlinkscore.why" /> <span><FormattedMessage id="seedlinkscore.pro" /></span> ?</p>
                            <p className="content"><FormattedMessage id="seedlinkscore.why_pro" />
                            </p>
                        </div>
                    </div>
                    <div className="cards enterprise">
                        <p className="card-title"><FormattedMessage id="seedlinkscore.enterprise_full" /></p>
                        <div className="items">
                            <span className="item"><FormattedMessage id="seedlinkscore.pro_full" /></span>
                            <span className="item"><FormattedMessage id="seedlinkscore.description_enterprise" /></span>
                        </div>
                        <div className="why-choose">
                            <p><FormattedMessage id="seedlinkscore.why" /> <span><FormattedMessage id="seedlinkscore.enterprise" /></span> ?</p>
                            <p className="content"><FormattedMessage id="seedlinkscore.why_enterprise" /></p>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
export default SlscorePop;
