import React from 'react';
import { FormattedMessage } from 'react-intl';
// import './demoResultBar.scss';

class DemoResultBar extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            // myScore:this.props.myScore,
            // startScore:this.props.startScore,
            // endScore:this.props.endScore,
            // scoreProperty:this.props.scoreProperty,
        };
    }
    render () {
        const circleLeft = this.props.myScore - 1;
        const circleStyle = {
            left: `${circleLeft}%`,
        };
        const showBarWidth = this.props.endScore - this.props.startScore;
        const showBarStyle = {
            width: `${showBarWidth}%`,
            marginLeft: `${this.props.startScore}%`,
        };
        return (
            <div className="resultBar">
                <p className="barTitle"><FormattedMessage id={this.props.scoreProperty} /></p>
                <div className="wholeBar">
                    <div className="scoreCircle" style={circleStyle}>
                        <span>{this.props.myScore}</span>
                    </div>
                    <div className="showBar" style={showBarStyle}>
                        <span className="label-1">{this.props.startScore}</span>
                        <span className="label-2">{this.props.endScore}</span>
                    </div>
                </div>

            </div>
        );
    }
}
DemoResultBar.propTypes = {
    myScore: React.PropTypes.number,
    startScore: React.PropTypes.number,
    endScore: React.PropTypes.number,
    scoreProperty: React.PropTypes.string,
};
DemoResultBar.defaultProps = {
    myScore: 0,
    startScore: 0,
    endScore: 0,
    scoreProperty: '',
};
export default DemoResultBar;
