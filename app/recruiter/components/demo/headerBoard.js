import React from 'react';
// import './headerBoard.scss';
import { FormattedMessage } from 'react-intl';

class headerBoard extends React.Component {
    constructor (props) {
        super(props);
        this.jumpLogin = this.jumpLogin.bind(this);
        this.jumpSignup = this.jumpSignup.bind(this);
    }
    jumpLogin () {
        window.location.href = this.props.loginUrl;
    }
    jumpSignup () {
        window.location.href = this.props.signupUrl;
    }
    render () {
        // <button className="signup" onClick={this.jumpSignup}>
        //     <FormattedMessage id="hr.signup" />
        // </button>
        return (
            <div className="headerBoard">
                <div className="header-container">
                    <a href="http://www.seedlinktech.com/">
                        <div className="logo" href="http://www.seedlinktech.com/" />
                    </a>
                    <div className="links">
                        <a href="http://www.seedlinktech.com/" target="_blank">
                            <FormattedMessage id="hr.about" />
                        </a>
                    </div>
                    <div className="contact-us">
                        <a href="tel:+86-21-6111-6264"><div><i className="iconfont icon-phone" />+86 (21) 6111 6264</div></a>
                        <a href="mailto:info@seedlinktech.com"><div><i className="iconfont icon-mail" />info@seedlinktech.com</div></a>
                    </div>
                    <div className="signups">
                        <button className="login" onClick={this.jumpLogin}>
                            <FormattedMessage id="hr.signin" />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}
headerBoard.propTypes = {
    loginUrl: React.PropTypes.string,
    signupUrl: React.PropTypes.string,
};
headerBoard.defaultProps = {
    loginUrl: 'https://www.rcxue.com/?login=true',
    signupUrl: 'https://www.rcxue.com/?signup=true',
};


export default headerBoard;
