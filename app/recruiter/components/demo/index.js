export HeaderBanner from './headerBanner';
export HeaderBoard from './headerBoard';
export DemoResultBar from './demoResultBar';
export DonutPie from './donutPie';
export DemoSubmit from './demoSubmit';
export SlscorePop from './slscorePop';
export DemoFooter from './demoFooter';
