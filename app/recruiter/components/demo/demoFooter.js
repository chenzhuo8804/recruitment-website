import React from 'react';
// import './demoFooter.scss';


class DemoFooter extends React.Component {
    constructor (props) {
        super(props);
    }
    render () {
        return (
            <div className="demoFooter">
                <p className="copywrite">&#169;2016 SeedlinkTech</p>
                <p>沪ICP备13034759号－2</p>
            </div>
        );
    }
}
export default DemoFooter;
