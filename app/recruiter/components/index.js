import IconFont from './iconfont';
import List from './list';
import ListItem from './listItem';


import App from './app';
import Home from './home';
import CompanyInfo from './companyInfo';
import Demo from './demo';
import Feedback from './feedback';
import Rating from './rating';

export {
    IconFont,
    List,
    ListItem,
    App,
    Home,
    CompanyInfo,
    Demo,
    Feedback,
    Rating,
};

export default {
    IconFont,
    List,
    ListItem,
    App,
    Home,
    CompanyInfo,
    Demo,
    Feedback,
    Rating,
};
