import React, { Component, PropTypes } from 'react';
import { injectIntl, intlShape } from 'react-intl';
import classNames from 'classnames/dedupe';

import { IconFont } from '../../components';

const propTypes = {
    className: PropTypes.string,
    intl: intlShape.isRequired,
};

class Search extends Component {
    constructor (props) {
        super(props);
        this.state = {
        };
    }
    render () {
        const { className } = this.props;
        const { formatMessage } = this.props.intl;

        return (
            <div className={classNames('app-search', className)}>
                <div className="search-input">
                    <input
                        className="input"
                        placeholder={formatMessage({ id: 'hint_search_bar' })}
                    />
                </div>
                <div className="search-button">
                    <button className="button"><IconFont iconName="icon-search" /></button>
                </div>
            </div>
        );
    }
}

Search.propTypes = propTypes;

export default injectIntl(Search);
