import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import classNames from 'classnames/dedupe';

import { IconFont } from '../../components';

const propTypes = {
    className: PropTypes.string,
};

class Account extends Component {

    constructor (props) {
        super(props);

        this.state = {
            isShow: false,
        };

        this.toggleSettings = this.toggleSettings.bind(this);
        this.hideSettings = this.hideSettings.bind(this);
    }

    toggleSettings (e) {
        this.setState(prevState => ({
            isShow: !prevState.isShow,
        }));
        e.stopPropagation();
    }

    hideSettings (e) {
        this.setState(() => ({
            isShow: false,
        }));
        e.stopPropagation();
    }

    render () {
        const { className } = this.props;

        return (
            <div className={classNames('app-account', className)} onBlur={this.hideSettings}>
                <button className="account-avatar" onClick={this.toggleSettings}>
                    <span className="avatar" />
                </button>
                <ul className={classNames('account-settings', { show: this.state.isShow })}>
                    <li className="setting">
                        <IconFont iconName="icon-setting" />
                        <FormattedMessage id="hr.account_settings" />
                    </li>
                    <li className="setting">
                        <IconFont iconName="icon-quit" />
                        <FormattedMessage id="button.logout" />
                    </li>
                </ul>
            </div>
        );
    }
}

Account.propTypes = propTypes;

export default Account;
