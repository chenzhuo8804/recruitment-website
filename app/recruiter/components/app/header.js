import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/dedupe';

import { IconFont } from '../../components';
import { Search, Account, Help } from '../app';
import Constant from '../../constant';

const propTypes = {
    className: PropTypes.string,
    hasSearch: PropTypes.bool.isRequired,
    hasAccount: PropTypes.bool.isRequired,
    hasHelp: PropTypes.bool.isRequired,
    hasContact: PropTypes.bool.isRequired,
};

class Header extends Component {
    constructor (props) {
        super(props);
        this.state = {
        };
    }

    render () {
        const { className, hasSearch, hasAccount, hasHelp, hasContact } = this.props;
        const { homeSite, contactPhone, contactMail } = Constant;
        return (
            <div className={classNames('app-header', className)}>
                <div className="header-logo">
                    <a className="logo" href={homeSite}>
                        <IconFont iconName="icon-logo" />
                    </a>
                </div>
                <div className="header-body">
                    { hasContact &&
                        <div className="header-contact">
                            <a className="contact" href={`tel:${contactPhone}`}>
                                <IconFont iconName="icon-contact icon-phone" />
                                <span className="phone-num">{contactPhone}</span>
                            </a>
                            <a className="contact" href={`mailto:${contactMail}`}>
                                <IconFont iconName="icon-contact icon-mail" />
                                <span className="email-address">{contactMail}</span>
                            </a>
                        </div>
                    }
                    { hasSearch && <Search className="header-search" /> }
                </div>
                <div className="header-function">
                    { hasAccount && <Account className="function" /> }
                    { hasHelp && <Help className="function" /> }
                </div>
            </div>
        );
    }
}

Header.propTypes = propTypes;

export default Header;
