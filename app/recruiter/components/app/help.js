import React, { Component, PropTypes } from 'react';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';
import classNames from 'classnames/dedupe';

import { IconFont } from '../../components';

const propTypes = {
    className: PropTypes.string,
    intl: intlShape.isRequired,
};

class Help extends Component {

    constructor (props) {
        super(props);

        this.state = {
            showHelp: false,
        };

        this.toggleHelp = this.toggleHelp.bind(this);
        this.showHelp = this.showHelp.bind(this);
        this.hideHelp = this.hideHelp.bind(this);
    }

    toggleHelp (e) {
        this.setState(prevState => ({
            isShow: !prevState.isShow,
        }));

        e.stopPropagation();
    }

    showHelp (e) {
        this.setState(() => ({
            isShow: true,
        }));

        e.stopPropagation();
    }

    hideHelp (e) {
        const relatedTarget = e.relatedTarget;

        this.setState(() => ({
            isShow: relatedTarget === this.helpInput,
        }));

        e.stopPropagation();
    }

    render () {
        const { className } = this.props;
        const { formatMessage } = this.props.intl;

        return (
            <div className={classNames('app-help', className)} onBlurCapture={this.hideHelp}>
                <button className="help-button" onClick={this.toggleHelp}>
                    <IconFont iconName="icon-help" />
                </button>
                <div className={classNames('help-dialog', { show: this.state.isShow })}>
                    {/* <div className="help-title">
                        <FormattedMessage id="button.help" />
                    </div> */}
                    <textarea
                        className="help-input"
                        placeholder={formatMessage({ id: 'hint.help' })}
                        onFocus={this.showHelp}
                        ref={(input) => { this.helpInput = input; }}
                    />
                    <button className="help-submit">
                        <FormattedMessage id="button.help" />
                    </button>
                </div>
            </div>
        );
    }
}

Help.propTypes = propTypes;

export default injectIntl(Help);
