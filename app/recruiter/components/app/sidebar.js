import React, { Component, PropTypes } from 'react';

import { IconFont } from '../../components';

const propTypes = {
    className: PropTypes.string.isRequired,
};

class Sidebar extends Component {

    constructor (props) {
        super(props);

        this.state = {
            isLogin: false,
        };
    }

    render () {
        return (
            <nav className={this.props.className}>
                <ul className="nav-list">
                    <li className="nav-item">
                        <a href="#" className="nav nav-button">
                            <IconFont iconName="icon-create" />
                            <span className="text">Create</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav nav-link nav-active">
                            <IconFont iconName="icon-dashboard" />
                            <span className="text">Dashboard</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav nav-link">
                            <IconFont iconName="icon-pi-volume" />
                            <span className="text">PI Volumn</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav nav-link">
                            <IconFont iconName="icon-pi-pro" />
                            <span className="text">PI Pro</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav nav-link">
                            <IconFont iconName="icon-pi-enterprise" />
                            <span className="text">PI Enterprise</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav nav-link">
                            <IconFont iconName="icon-company" />
                            <span className="text">Company Info</span>
                        </a>
                    </li>
                </ul>
            </nav>
        );
    }
}

Sidebar.propTypes = propTypes;

export default Sidebar;
