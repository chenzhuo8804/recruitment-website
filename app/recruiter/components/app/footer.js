import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/dedupe';

import { IconFont } from '../../components';
import Constant from '../../constant';

const propTypes = {
    className: PropTypes.string,
    hasContact: PropTypes.bool.isRequired,
};

class Footer extends Component {
    constructor (props) {
        super(props);
        this.state = {
        };
    }

    render () {
        const { className, hasContact } = this.props;
        const { contactPhone, contactMail, copyright, icpCode } = Constant;

        return (
            <div className={classNames('app-footer', className)}>
                <div className="footer-body">
                    <div className="footer-left">
                        { hasContact &&
                            <div className="footer-contact">
                                <a className="contact" href={`tel:${contactPhone}`}>
                                    <IconFont iconName="icon-contact icon-phone" />
                                    <span className="phone-num">{contactPhone}</span>
                                </a>
                                <a className="contact" href={`mailto:${contactMail}`}>
                                    <IconFont iconName="icon-contact icon-mail" />
                                    <span className="email-address">{contactMail}</span>
                                </a>
                            </div>
                        }
                    </div>
                    <div className="footer-center">
                        <div className="footer-copyright">
                            <span className="phone-num">{copyright}</span>
                        </div>
                        { !hasContact &&
                            <div className="footer-icp">
                                <span className="phone-num">{icpCode}</span>
                            </div>
                        }
                    </div>
                    <div className="footer-right">
                        { hasContact &&
                            <div className="footer-icp">
                                <span className="phone-num">{icpCode}</span>
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

Footer.propTypes = propTypes;

export default Footer;
