import React from 'react';
// import './seedlinkFooter.scss';


class SeedlinkFooter extends React.Component {

    render () {
        return (
            <footer className="seedlinkFooter">
                <div className="hr-main">
                    <p className="phone"><i className="iconfont icon-dianhua" /><a href="tel:+86-21-6111-6264">(86 21)605 28 208</a></p>
                    <p className="xinfeng"><i className="iconfont icon-xinfeng" /><a href="mailto:info@seedlinktech.com">info@seedlinktech.com</a></p>
                    <p className="right">—沪ICP13034759号-2—</p>
                </div>
            </footer>
        );
    }
}
export default SeedlinkFooter;
