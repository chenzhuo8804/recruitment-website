import React from 'react';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';

// import '../../base.scss';
// import './seedlinkHeader.scss';
import Request from 'superagent';
import Cookie from '../../../helpers/cookie';


class SeedlinkHeader extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isShowSettingBox: false,
            isShowFeedbackBox: false,
            isEnterFeedBack: false,
            isValidate: false,
            isPostFeedBackData: false,
            isReurnDataOk: false,
            isShowAvatar: false,
            isLogin: false,
            feedbackDescription: '',
            avatarUrl: '',
            avatarWidth: '',
            avatarHeight: '',
            avatarTop: '',
            avatarLeft: '',
        };
        this.handleAvatarClick = this.handleAvatarClick.bind(this);
        this.handleFeedBackClick = this.handleFeedBackClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }
    componentWillMount () {
        this.getUserId();
    }

    getUserId () {
        Request
        .get('/api/v2/user/me/')
        .end((err, res) => {
            if (err || !res.ok) {
                console.log('noLogin');
            } else if (res) {
                const data = JSON.parse(res.text);
                this.setState({
                    isLogin: true,
                });
                this.getUserInfo(data.id);
            }
        });
    }

    getUserInfo (uid) {
        const that = this;
        Request
        .get(`/api/v1/user/${uid}/show/`)
        .end((err, res) => {
            if (err || !res.ok) {
                console.log('wrong');
            } else if (res) {
                const data = JSON.parse(res.text);

                const img = new Image();
                img.src = data.avatar;
                if (img.complete) {
                    that.fitImageIntoContainer(img, 26, 26, 'cover');
                    if (img.width) {
                        that.setState({
                            avatarUrl: data.avatar,
                        });
                    }
                } else {
                    img.onload = function () {
                        that.fitImageIntoContainer(img, 26, 26, 'cover');
                    };
                    if (img.width) {
                        that.setState({
                            avatarUrl: data.avatar,
                        });
                    }
                }
            }
        });
    }

    fitImageIntoContainer (image, _displayW, _displayH, sizeType) {
        const isCoverMode = sizeType === 'cover';
        const _iW = image.width;
        const _iH = image.height;

        const _cW = _displayW;
        const _cH = _displayH;

        const pictureRatio = _iW / _iH;
        const containerRatio = _cW / _cH;

        let imgWidth = '';
        let imgHeight = '';
        let imgTop = '';
        let imgLeft = '';


        if (pictureRatio > containerRatio) {
            if (isCoverMode) {
                _displayW = _displayH * pictureRatio;
            } else {
                _displayH = _displayW / pictureRatio;
            }
            imgWidth = `${_displayW}px`;
            imgHeight = `${_displayH}px`;
        } else {
            if (isCoverMode) {
                _displayH = _displayW / pictureRatio;
            } else {
                _displayW = _displayH * pictureRatio;
            }
            imgWidth = `${_displayW}px`;
            imgHeight = `${_displayH}px`;
        }
        if (
            (pictureRatio > containerRatio && isCoverMode) ||
            (pictureRatio <= containerRatio && !isCoverMode)
        ) {
            imgLeft = `${(_cW - _displayW) / 2}px`;
            imgTop = '0';
        }

        if (
            (pictureRatio <= containerRatio && isCoverMode) ||
            (pictureRatio > containerRatio && !isCoverMode)
        ) {
            imgLeft = '0';
            imgTop = `${(_cH - _displayH) / 2}px`;
        }
        this.setState({
            avatarWidth: imgWidth,
            avatarHeight: imgHeight,
            avatarTop: imgTop,
            avatarLeft: imgLeft,
        });
    }


    handleAvatarClick () {
        if (this.state.isShowSettingBox) {
            this.setState({
                isShowSettingBox: false,
            });
        }
        if (!this.state.isShowSettingBox) {
            this.setState({
                isShowSettingBox: true,
            });
        }
    }
    handleFeedBackClick () {
        if (this.state.isShowFeedbackBox) {
            this.setState({
                isShowFeedbackBox: false,
            });
        }
        if (!this.state.isShowFeedbackBox) {
            this.setState({
                isShowFeedbackBox: true,
            });
        }
        if (this.state.isShowSettingBox) {
            this.setState({
                isShowSettingBox: false,
            });
        }
    }
    handleChange (event) {
        if (event.target.value) {
            this.setState({
                isEnterFeedBack: true,
                feedbackDescription: event.target.value,
            });
        } else {
            this.setState({
                isEnterFeedBack: false,
            });
        }
        this.setState({
            isPostFeedBackData: false,
        });
    }

    handleLogout () {
        Request
        .get('/force_logout/')
        .end((err, res) => {
            if (err || !res.ok) {
                console.log('wrong');
            } else {
                window.location.href = '/home';
            }
        });
    }

    handleDelete () {
        this.setState({
            isShowFeedbackBox: false,
        });
    }

    handleSubmit () {
        this.setState({
            isValidate: true,
        });
        if (this.state.isEnterFeedBack) {
            this.setState({
                isPostFeedBackData: true,
            });
            const data = new Object();
            data.description = this.state.feedbackDescription;
            data.referer = document.referrer;
            data.faq_type = 5;

            Request
            .post('/api/v2/feedback/')
            .send(data)
            .set('X-CSRFToken', Cookie.get('csrftoken'))
            .end((err, res) => {
                if (err || !res.ok) {
                    this.setState({
                        isReurnDataOk: false,
                    });
                } else {
                    this.setState({
                        isReurnDataOk: true,

                    });
                    setTimeout(
                        () => {
                            this.setState({
                                isShowFeedbackBox: false,
                                isValidate: false,
                                isPostFeedBackData: false,
                                isReurnDataOk: false,
                                isEnterFeedBack: false,
                            });
                        },
                        1000,
                    );
                }
            });
        }
    }

    render () {
        if (this.state.avatarUrl) {
            imgStyle = {
                width: this.state.avatarWidth,
                height: this.state.avatarHeight,
                top: this.state.avatarTop,
                left: this.state.avatarLeft,
                position: 'absolute',
            };
        }
        const {formatMessage} = this.props.intl;
        return (
            <header className="seedlinkHeader">
                <h1><i className="icon-seedlink iconfont" /><span>人才学</span></h1>
                {this.state.isLogin ?
                    <div className="userAvatar">
                        <div className="avarar-wrapper">
                            {this.state.avatarUrl ?
                                <img src={this.state.avatarUrl} onClick={this.handleAvatarClick} style={imgStyle} /> :
                                <i className="defaultImg" onClick={this.handleAvatarClick} />
                        }
                        </div>

                        {this.state.isShowSettingBox ?
                            <ul className="clickShow animated fadeIn">
                                <li>
                                    <a href="/drago/company/info/">
                                        <i className="iconfont icon-setting" />
                                        <FormattedMessage id="hr.account_settings" />
                                    </a>
                                </li>
                                <li>
                                    <a onClick={this.handleLogout}>
                                        <i className="iconfont icon-quit" />
                                        <FormattedMessage id="button.logout" />
                                    </a>
                                </li>
                            </ul> : null
                    }
                    </div> : null
                }
                {this.state.isLogin ?
                    <div className="help">
                        <div className="icon-wrapper" onClick={this.handleFeedBackClick}>
                            <i className="icon-wenhao iconfont" />
                            <div className="hoverShow" ><FormattedMessage id="button.help" /></div>
                        </div>
                        {!this.state.isReurnDataOk && this.state.isShowFeedbackBox ?
                            <div className="feedbackPopup animated fadeIn">
                                <div className="enterInfo">
                                    <FormattedMessage id="button.help" />
                                    <i className="iconfont icon-delete" onClick={this.handleDelete} />
                                    <textarea placeholder={formatMessage({ id: 'hint.help' })} onChange={this.handleChange} />
                                    <button onClick={this.handleSubmit}><FormattedMessage id="button.submit" /></button>
                                    {!this.state.isEnterFeedBack && this.state.isValidate ?
                                        <em id="enter-error" className="animated fadeIn">
                                            <FormattedMessage id="js.feedback.problem_category.0" />
                                        </em> : null
                                }
                                    {!this.state.isReurnDataOk && this.state.isPostFeedBackData ?
                                        <em id="submit-error" className="animated fadeIn">
                                            <FormattedMessage id="error.submit_failed" />
                                        </em> : null
                                }
                                </div>
                            </div> : null
                    }
                        {this.state.isReurnDataOk && this.state.isShowFeedbackBox ?
                            <div className="feedbackPopup animated fadeOut">
                                <div className="submitSuccess">
                                    <FormattedMessage id="js.feedback.problem_thankyou" />
                                </div>
                            </div> : null
                    }
                        <div className="clickShow" />
                    </div> : null
                }
            </header>
        );
    }
}
SeedlinkHeader.contextTypes = {
    intl: intlShape.isRequired,
}
export default injectIntl(SeedlinkHeader);
