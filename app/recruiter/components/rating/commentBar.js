import React from 'react';
// import './commentBar.scss';
import { FormattedMessage } from 'react-intl';

import Star from './star';

class CommentBar extends React.Component {
    constructor (props) {
        super(props);
    }

    changeColor (index) {
        this.setState({
            curLight: index,
        });
    }


    render () {
        return (
            <ul className="commentBar">
                <li>
                    <p className="name">{this.props.name}</p>
                    {this.props.status === 'notpass' ?
                        <p className="status notPass">
                            <FormattedMessage id="hr.not_pass" />
                        </p> : null
                    }
                    {this.props.status === 'pending' ?
                        <p className="status pending">
                            <FormattedMessage id="hr.pending" />
                        </p> : null
                    }
                    {this.props.status === 'pass' ?
                        <p className="status pass">
                            <FormattedMessage id="hr.pass" />
                        </p> : null
                    }
                </li>
                <li className="behavior">{
                    this.props.behavior.map((data, index) =>
                        <Star total={5} current={data.score} isChangeColor={false} name={data.trait.name} key={index} id={data.id} />,
                    )}
                </li>
                <li className="description">
                    {this.props.commont}
                </li>
            </ul>
        );
    }
}
export default CommentBar ;
