import React from 'react';
// import './star.scss';
import { FormattedMessage } from 'react-intl';


class Star extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            curLight: this.props.current,
            allLength: this.props.total,
            isChangeColor: this.props.isChangeColor,
            name: this.props.name,
            id: this.props.id,
            changeColorTime: 0,
        };
    }

    changeColor (curLight) {
        const clickTime = this.state.changeColorTime + 1;

        if (this.state.isChangeColor) {
            this.setState({
                curLight,
                changeColorTime: clickTime,
            });
        }

        if (clickTime !== 1) {
            this.props.callbackParent(0, this.state.id, curLight);
        } else {
            this.props.callbackParent(1, this.state.id, curLight);
        }
    }

    render () {
        const newArry = [];
        for (let i = 0; i < this.state.allLength; i++) {
            newArry.push(
                <i onClick={this.changeColor.bind(this, i + 1)} key={i} className={this.state.curLight > i ? 'cur' : ''} />,
            );
        }

        return (
            <p className="star" ><FormattedMessage id={this.state.name} /><em>{newArry}</em></p>
        );
    }
}

Star.propTypes = {
    callbackParent: React.PropTypes.func,
    name: React.PropTypes.string,
    id: React.PropTypes.number,
    isChangeColor: React.PropTypes.bool,
    total: React.PropTypes.number,
    current: React.PropTypes.number,
};

export default Star;
