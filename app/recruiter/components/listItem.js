import React from 'react';
import { FormattedMessage } from 'react-intl';
// import './listItem.scss';

class ListItem extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            // isHovered: false,
        };
    }
    // mouseEnterEvent(){
    //     this.setState({isHovered: true});
    // }
    // mouseLeaveEvent(){
    //     this.setState({isHovered: false});
    // }

    render () {
        // let liStyle = {};
        // if (this.props.isSelected) {
        //     liStyle['background'] = this.props.selectedColor;
        //     liStyle['color'] = '#fff';
        // }
        // if (this.state.isHovered) {
        //     liStyle['background'] = this.props.hoveredColor;
        //     liStyle['color'] = '#fff';
        // }


        return (
            <li
              onClick={this.props.onClick}
              className={this.props.curr}
            ><FormattedMessage id={this.props.name} />
            </li>
        );
    }
}
ListItem.propTypes = {
    onClick: React.PropTypes.func.isRequired,
    isSelected: React.PropTypes.bool,
    name: React.PropTypes.string,
    // selectedColor: React.PropTypes.string,
    curr: React.PropTypes.string,
    // hoveredColor: React.PropTypes.string,
    // liBackgroundColor: React.PropTypes.string,
};
ListItem.defaultProps = {
    isSelected: false,
};
export default ListItem;
