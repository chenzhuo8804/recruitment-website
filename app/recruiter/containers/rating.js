import React from 'react';
import Request from 'superagent';
import { FormattedMessage } from 'react-intl';

import { Star, CommentBar, SeedlinkFooter, SeedlinkHeader } from '../components/rating';
import Cookie from '../../helpers/cookie';
// import './index.scss';

class Rating extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            token: Cookie.get('csrftoken'),
            tabIndex: parseInt(this.props.params.tabIndex, 0),
            isDisableSaveBtn: false,
            isDisableSubmitBtn: false,
            isSave: false,
            isSubmit: false,
            isShowSaveOK: false,
            isShowSaveFail: false,
            isShowSubmitOk: false,
            isShowSubmitFail: false,
            isServerError: false,
            finialSelect: 0,
            finialState: 0,
            scoreNum: 0,
            traitLength: 0,
            commentsNum: 0,
            getScoreData: [],
            postScoreData: [],
            reviewData: [],
            reviewerName: '',
            resultSelect: '',
            comment: '',
            CandidateUrl: '',
            PositionUrl: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFinialSelect = this.handleFinialSelect.bind(this);
        this.shareBackUrl = this.shareBackUrl.bind(this);
        this.handleTabClick = this.handleTabClick.bind(this);
        this.updateInputName = this.updateInputName.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.updateCommentValue = this.updateCommentValue.bind(this);
        this.onScoreChanged = this.onScoreChanged.bind(this);
        this.updateCommentValue = this.updateCommentValue.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    componentWillMount () {
        let candidateUrl = null;
        let positionUrl = null;
        let midUrl = '';
        const pid = this.props.params.pid;
        const uid = this.props.params.viewingUid;
        const page = this.props.params.page;
        const myRank = this.props.params.myRank;
        const allRank = this.props.params.allRank;
        const shareToken = this.props.params.shareToken;
        const mid = parseInt(this.props.params.mid, 0);

        if (mid) {
            positionUrl = '/people-insights/';
            midUrl = `&mid=${parseInt(this.props.params.mid, 0)}`;
        } else {
            positionUrl = '/questmatch/';
        }
        candidateUrl = `/drago/candidate/personal/info/?uid=${uid}&pid=${pid}&myRank=${myRank}&allRank=${allRank}&page=${page}${midUrl}`;

        this.getTrait(pid, shareToken);
        this.getStatus(pid, uid, shareToken);
        if (!shareToken) {
            this.getReview(pid, uid, shareToken);
        }
        this.setState({
            CandidateUrl: candidateUrl,
            PositionUrl: positionUrl,
        });
    }


    getTrait (pid, shareToken) {
        const data = [];
        const that = this;

        const getTraitUlr = `/api/v2/review/position/${pid}/traits/?share_token=${shareToken}`;

        Request
        .get(getTraitUlr)
        .send(data)
        .end((err, res) => {
            if (err || !res.ok) {
                console.log('Failed to get trait');
            } else if (res.status === 200) {
                that.setState({
                    getScoreData: JSON.parse(res.text),
                    traitLength: JSON.parse(res.text).length,
                });
            } else {
                that.setState({ isServerError: true });
                console.log('Failed to get trait');
            }
        });
    }
    getStatus (pid, viewinguid, shareToken) {
        const postData = {
            position_id: pid,
            user_id_list: [viewinguid],
        };
        const that = this;
        const getStatusUrl = `/api/v1/get/candidates/application/status/?token=${shareToken}`;
        Request
        .post(getStatusUrl)
        .send(postData)
        .set('X-CSRFToken', this.state.token)
        .end((err, res) => {
            if (err || !res.ok) {
                console.log('Fail to get status');
            } else if (res.status === 200) {
                const data = JSON.parse(res.text);
                that.setState({
                    finialState: data[0].status,
                    finialSelect: parseInt(data[0].status),
                });
            } else {
                that.setState({ isServerError: true });
                console.log('Fail to get status');
            }
        });
    }

    getReview (pid, viewinguid, shareToken) {
        const getUrl = `/api/v2/review/position/${pid}/candidate/${viewinguid}/`;
        const that = this;
        Request
        .get(getUrl)
        .set('X-API-Key', 'foobar')
        .set('Accept', 'application/json')
        .end((err, res) => {
            if (err || !res.ok) {
                console.log('Fail to get candidate reviewData');
            } else if (res.status === 200) {
                var rData = JSON.parse(res.text);
                rData.sort(function(a, b){
                    return a.created_at < b.created_at ? 1 : -1;
                });
                that.setState({
                    reviewData: rData,
                    commentsNum: JSON.parse(res.text).length,
                });
            } else {
                that.setState({ isServerError: true });
                console.log('Fail to get candidate reviewData');
            }
        });

        this.setState({

        });
    }


    shareBackUrl () {
        window.location.href = `/drago/candidates/sharing/?token=${this.props.params.shareToken}&pid=${this.props.params.pid}`;
    }

    handleTabClick (event) {
        this.setState({
            tabIndex: parseInt(event.target.getAttribute('data-id')),
        });
    }

    handleSelect (event) {
        const value = event.target.getAttribute('data-value');
        this.setState({
            resultSelect: value,
        });
    }

    handleFinialSelect (event) {
        const value = event.target.getAttribute('data-id');
        this.setState({
            finialSelect: parseInt(value),
        });
    }

    updateInputName (event) {
        this.setState({
            reviewerName: event.target.value,
        });
    }

    updateCommentValue (event) {
        this.setState({
            comment: event.target.value,
        });
    }

    onScoreChanged (scoreNum, id, score) {
        const postScoreData = this.state.postScoreData;
        if (scoreNum) {
            postScoreData.push({
                score,
                trait_id: id,
            });
        } else {
            for (let i = 0; i < this.state.postScoreData.length; i++) {
                if (this.state.postScoreData[i].trait_id === id) {
                    this.state.postScoreData[i].score = score;
                }
            }
        }
        this.setState({
            scoreNum: this.state.scoreNum + scoreNum,
            postScoreData,
        });
    }

    handleSave () {
        let verification = false;
        const postUrl = `/api/v2/review/position/${this.props.params.pid}/candidate/${this.props.params.viewingUid}/?share_token=${this.props.params.shareToken}`;
        this.setState({
            isSave: true,
            isDisableSaveBtn: true,
        });
        if (!this.state.finialState < 19) {
            verification = this.state.reviewerName !== '' && this.state.scoreNum >= this.state.traitLength;
        } else {
            verification = this.state.reviewerName !== '' && !this.state.resultSelect && this.state.scoreNum >= this.state.traitLength;
        }
        if (verification) {
            const data = {
                reviewer_name: this.state.reviewerName,
                comment: this.state.comment,
                recommendation: this.state.resultSelect,
                scores: this.state.postScoreData,
            };

            Request
            .post(postUrl)
            .send(data)
            .set('X-CSRFToken', this.state.token)
            .end((err, res) => {
                if (err || !res.ok) {
                    this.setState({ isShowSaveFail: true });
                    setTimeout(
                        () => {
                            this.setState({ isShowSaveFail: false });
                        },
                        1500,
                    );
                } else if (res.status === 200) {
                    this.setState({ isShowSaveOk: true });
                    if (this.props.params.shareToken) {
                        setTimeout(
                            () => {
                                window.location.href = `/drago/candidates/sharing/?token=${this.props.params.shareToken}&pid=${this.props.params.pid}`;
                            },
                            1500,
                        );
                    } else {
                        setTimeout(
                            () => {
                                if (!this.props.params.shareToken) {
                                    this.setState({
                                        tabIndex: 1,
                                    });
                                }
                                this.setState({ isShowSaveOk: false });
                            },
                            1500,
                        );
                        const returnData = this.state.reviewData;
                        returnData.push(JSON.parse(res.text));
                        returnData.sort(function(a, b){
                            return a.created_at < b.created_at ? 1 : -1;
                        });
                        this.setState({
                            reviewData: returnData,
                            commentsNum: this.state.commentsNum + 1,
                        });
                    }
                } else {
                    this.setState({ isShowSaveFail: true });
                    setTimeout(
                        () => {
                            this.setState({ isShowSaveFail: false });
                        },
                        1500,
                    );
                }
                this.setState({
                    isDisableSaveBtn: false,
                });
            });
        } else {
            this.setState({
                isDisableSaveBtn: false,
            });
        }
    }

    handleSubmit () {
        const result = this.state.finialSelect;
        this.setState({
            isSubmit: true,
            isDisableSubmitBtn: true,
        });

        if (result) {
            const data = {
                status: result,
                position_id: this.props.params.pid,
                user_id_list: [this.props.params.viewingUid],
            };
            Request
            .post('/api/v1/user/applications/update/')
            .send(data)
            .set('X-CSRFToken', this.state.token)
            .end((err, res) => {
                if (err || !res.ok) {
                    this.setState({ isShowSubmitFail: true });
                    setTimeout(
                        () => {
                            this.setState({ isShowSubmitFail: false });
                        },
                        1500,
                    );
                } else if (res.status === 200) {
                    this.setState({ isShowSubmitOk: true });
                    setTimeout(
                        () => {
                            this.setState({ isShowSubmitOk: false, finialState: result });
                        },
                        1500,
                    );
                } else {
                    this.setState({ isShowSubmitFail: true });
                    setTimeout(
                        () => {
                            this.setState({ isShowSubmitFail: false });
                        },
                        1500,
                    );
                }
                setTimeout(
                    () => {
                        this.setState({
                            isDisableSubmitBtn: false,
                        });
                    },
                    1500,
                );
            });
        } else {
            this.setState({
                isDisableSubmitBtn: false,
            });
        }
    }

    render () {
        return (
            <div className="rating-wrapper">
                <div className="rating">
                    <SeedlinkHeader />
                    <div className="hr-main">
                        {this.props.params.shareToken ?
                            <div className="returnBtn">
                                <a onClick={this.shareBackUrl} ><i className="iconfont icon-fanhui" /></a>
                            </div>
                            :
                            <nav className="breadcrumbs">
                                <ul>
                                    <li>
                                        <a href={this.state.PositionUrl} id="positionUrl">
                                            {parseInt(this.props.params.mid, 0) ?
                                                <FormattedMessage id="People insights" /> :
                                                <FormattedMessage id="QuestMatch" />
                                            }
                                            &gt;
                                            </a>
                                    </li>
                                    <li>
                                        <a href={`/drago/position/${this.props.params.pid}/rank/?page=${this.props.params.page}`} id="rankUrl">
                                            <FormattedMessage id="hr.ranking" />&gt;
                                            </a>
                                    </li>
                                    <li>
                                        <a href={this.state.CandidateUrl} id="candidateUrl" >
                                            <FormattedMessage id="hr.candidate" />&gt;
                                            </a>
                                    </li>
                                    <li className="cur">
                                        <FormattedMessage id="hr.rating_review" />
                                    </li>
                                </ul>
                            </nav>
                        }
                        <div className="ratingNav">
                            <ul>
                                <li className={this.state.tabIndex === 0 ? 'cur' : ''}><i className="icon-bianji iconfont" /><FormattedMessage id="hr.rating" /><em onClick={this.handleTabClick} data-id="0" /></li>
                                {!this.props.params.shareToken ?
                                    <li className={this.state.tabIndex === 0 ? '' : 'cur'} ><i className="icon-chakan iconfont" /><FormattedMessage id="hr.review" /><em onClick={this.handleTabClick} data-id="1" /></li> : null
                                }
                            </ul>
                        </div>
                        <div className="ratingMain">
                            <div className={this.state.tabIndex === 0 ? 'cur ratingTab' : 'ratingTab'}>
                                <div className="mainWrapper">
                                    <div className="tab-left">
                                        <p className="tips">
                                            <FormattedMessage
                                              id="hr.rating_someone"
                                              values={{
                                                  user_name: <em>{this.props.params.candidateName} </em>,
                                              }}
                                            />
                                        </p>
                                        <div className="name">
                                            <FormattedMessage id="hr.your_name" tagName="label" />
                                            <input type="text" onChange={this.updateInputName} />
                                            { this.state.reviewerName === '' && this.state.isSave ?
                                                <p
                                                  className="errorShow fadeIn animated" id="name-error"
                                                >
                                                    <FormattedMessage
                                                      id="notify.complete_required_fields"
                                                    />
                                                </p> : null
                                            }

                                        </div>

                                        {this.state.finialState < 19 ?
                                            <div className="tab-result">
                                                <label><FormattedMessage id="hr.recommendation" /></label>
                                                <ul className="hover">
                                                    <li className={this.state.resultSelect === 'pass' ? 'cur' : ''} onClick={this.handleSelect} data-value="pass"><FormattedMessage id="hr.pass" /></li>
                                                    <li className={this.state.resultSelect === 'notpass' ? 'cur' : ''} onClick={this.handleSelect} data-value="notpass"><FormattedMessage id="hr.not_pass" /></li>
                                                    <li className={this.state.resultSelect === 'pending' ? 'cur' : ''} onClick={this.handleSelect} data-value="pending" ><FormattedMessage id="hr.pending" /></li>
                                                </ul>
                                                {!this.state.resultSelect && this.state.isSave ?
                                                    <p className="errorShow fadeIn animated" id="select-error"><FormattedMessage id="notify.complete_required_fields" /></p> : null
                                            }
                                            </div> :
                                            <p className="Comment">
                                                <FormattedMessage id="hr.comment_optional" tagName="label" />
                                                <textarea onChange={this.updateCommentValue} />
                                            </p>
                                        }
                                    </div>
                                    <div className="tab-right">
                                        <FormattedMessage id="hr.score" tagName="label" />
                                        <div id="behaviorScore">{
                                            this.state.getScoreData.map((data, index) =>
                                                <Star total={5} current={0} isChangeColor name={data.name} callbackParent={this.onScoreChanged} key={index} id={data.id} />,
                                            )}
                                        </div>
                                        { this.state.scoreNum < this.state.traitLength && this.state.isSave ?
                                            <p className="errorShow fadeIn animated" id="behavior-error"><FormattedMessage id="notify.complete_required_fields" /></p> : null
                                        }
                                    </div>
                                </div>
                                {this.state.finialState < 19 ?
                                    <p className="comment-big">
                                        <FormattedMessage id="hr.comment_optional" tagName="label" />
                                        <textarea onChange={this.updateCommentValue} value={this.state.comment} />
                                    </p> : null
                                }

                                <div className="buttonBox">
                                    {this.state.isShowSaveOk ?
                                        <p className="errorShow fadeIn animated" id="save-success"><FormattedMessage id="notify.save_successfully" /></p> : null
                                    }
                                    {this.state.isShowSaveFail ?
                                        <p className="errorShow fadeIn animated " id="save-error"><FormattedMessage id="notify.save_failed" /></p> : null
                                    }
                                    <button onClick={this.handleSave} id="save-btn" disabled={this.state.isDisableSaveBtn}><FormattedMessage id="button.save" /></button>
                                </div>
                            </div>
                            <div className={this.state.tabIndex === 0 ? 'reviewsTab' : 'cur reviewsTab'}>
                                <p className="tips">
                                    <FormattedMessage
                                      id="hr.rating_count"
                                      values={{
                                          rating_count: <em>{this.state.commentsNum} </em>,
                                      }}
                                    />
                                </p>
                                <div className="view-result">{
                                    this.state.reviewData.map((data, index) =>
                                        <CommentBar name={data.reviewer_name} key={index} status={data.recommendation} commont={data.comment} behavior={data.scores} />,
                                    )}
                                </div>
                                <div className="tab-result">
                                    <FormattedMessage id="hr.interview_result" tagName="label" />
                                    {this.state.finialState < 19 ?
                                        <ul className="hover">
                                            <li className={this.state.finialSelect === 19 ? 'cur' : ''} onClick={this.handleFinialSelect} data-id="19"><FormattedMessage id="hr.accept" /></li>
                                            <li className={this.state.finialSelect === 20 ? 'cur' : ''} onClick={this.handleFinialSelect} data-id="20"><FormattedMessage id="hr.reject" /></li>
                                        </ul> : null
                                    }
                                    {this.state.finialState === 19 ?
                                        <ul className="hover">
                                            <li className={this.state.finialSelect === 21 ? 'cur' : ''} onClick={this.handleFinialSelect} data-id="21"><FormattedMessage id="hr.hired" /></li>
                                            <li className={this.state.finialSelect === 22 ? 'cur' : ''} onClick={this.handleFinialSelect} data-id="22"><FormattedMessage id="hr.dismission" /></li>
                                        </ul> : null
                                    }
                                    {this.state.finialState === 20 ?
                                        <ul>
                                            <li className={this.state.finialSelect === 19 ? 'cur' : ''} data-id="19"><FormattedMessage id="hr.accept" /></li>
                                            <li className={this.state.finialSelect === 20 ? 'cur' : ''} data-id="20"><FormattedMessage id="hr.reject" /></li>
                                        </ul> : null
                                    }

                                    {this.state.finialState === 21 || this.state.finialState === 22 ?
                                        <ul>
                                            <li className={this.state.finialSelect === 21 ? 'cur' : ''} data-id="21"><FormattedMessage id="hr.hired" /></li>
                                            <li className={this.state.finialSelect === 22 ? 'cur' : ''} data-id="22"><FormattedMessage id="hr.dismission" /></li>
                                        </ul> : null
                                    }


                                    {this.state.finialSelect === 0 && this.state.isSubmit ?
                                        <p className="errorShow fadeIn animated" id="finial-error">
                                            <FormattedMessage id="notify.complete_required_fields" />
                                        </p> : null
                                    }
                                </div>
                                {this.state.finialState <= 19 ?
                                    <div className="buttonBox">
                                        {this.state.isShowSubmitFail ?
                                            <p className="errorShow fadeIn animated" id="submit-error">
                                                <FormattedMessage id="notify.save_failed" />
                                            </p> : null
                                        }
                                        {this.state.isShowSubmitOk ?
                                            <p
                                              className="errorShow fadeIn animated" id="submit-success"
                                            >
                                                <FormattedMessage id="notify.save_successfully" />
                                            </p> : null
                                        }
                                        <button onClick={this.handleSubmit} id="submit-btn">
                                            <FormattedMessage id="button.submit" />
                                        </button>
                                    </div> : null
                                }
                            </div>
                        </div>
                    </div>
                    <SeedlinkFooter />
                </div>
            </div>

        );
    }
}

export default Rating;

Rating.propTypes = {
    params: React.PropTypes.object,
};
