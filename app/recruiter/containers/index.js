import App from './app';
import Home from './home';
import NotFound from './notFound';
import CompanyInfo from './companyInfo';
import Demo from './demo';
import Feedback from './feedback';
import Rating from './rating';

export {
    App,
    Home,
    NotFound,
    CompanyInfo,
    Demo,
    Feedback,
    Rating,
};
