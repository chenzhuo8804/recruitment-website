import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import { Panel } from '../components/page';
import { Header, Sidebar, Footer } from '../components/app';

const layoutTypes = {
    Landing: 'app-landing',
    Admin: 'app-admin',
};

const propTypes = {
    route: PropTypes.object.isRequired,
    children: PropTypes.element,
};

class App extends Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { layout } = this.props.route;

        let headerProps;
        let footerProps;
        let contentProps;
        switch (layout) {
            case layoutTypes.Landing:
                headerProps = {
                    hasSearch: false,
                    hasAccount: false,
                    hasHelp: false,
                    hasContact: true,
                };
                contentProps = {
                    scrollable: false,
                };
                footerProps = {
                    hasContact: false,
                };
                break;
            case layoutTypes.Admin:
                headerProps = {
                    hasSearch: true,
                    hasAccount: true,
                    hasHelp: true,
                    hasContact: false,
                };
                contentProps = {
                    scrollable: true,
                };
                footerProps = {
                    hasContact: true,
                };
                break;
            default:
        }

        return (
            <div className={classNames('app', layout)}>
                <Header className="app-header" {...headerProps} />
                <Sidebar className="app-sidebar" />
                <Panel className="app-content" {...contentProps}>
                    {this.props.children}
                </Panel>
                <Footer className="app-footer" {...footerProps} />
            </div>
        );
    }
}

App.propTypes = propTypes;
App.layoutTypes = layoutTypes;

export default App;
