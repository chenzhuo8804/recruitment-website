import React from 'react';
import ReactDOM from 'react-dom';
import Request from 'superagent';
import { FormattedMessage } from 'react-intl';
// import './index.scss';
import Cookie from '../../helpers/cookie';
import { HeaderBanner, HeaderBoard, DemoResultBar, DonutPie, DemoSubmit, SlscorePop, DemoFooter } from '../components/demo';

class Demo extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isLoading: false,
            preparingOptions: false,
            showSlscorePop: false,
            isfetchingData: true,
            hasServerError: false,
            codePass: false,
            startCheckingCode: false,
            isCheckingCode: false,
            codeIsEmpty: false,
            codeAuthFail: false,
            invitationCode: '',
            competencys: [],
            slscore: null,
            slscoreBrief: '',
            competencyDes: [],
            experienceLevelDict: {},
            jobTypes: [],
            expLevels: [],
            jobTypesDict: {},
            optionsValue: [],
            applicationID: null,
        };
        this.toggleSlscore = this.toggleSlscore.bind(this);
        this.dismissSlscore = this.dismissSlscore.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.updateCode = this.updateCode.bind(this);
        this.checkCode = this.checkCode.bind(this);
        this.shouldSubmit = this.shouldSubmit.bind(this);
    }
    componentWillMount () {
        this.setState({
            preparingOptions: true,
        });
    }
    componentDidMount () {
        const self = this;
        Request
        .get(self.props.getOptions)
        .end((err, res) => {
            if (err || !res.ok) {
                self.setState({ hasServerError: true, preparingOptions: false });
            } else if (res.status === 200) {
                const responseJson = JSON.parse(res.text);
                const levels = responseJson.exp_level;
                const types = responseJson.job_type;
                self.setState({ jobTypes: types, expLevels: levels, preparingOptions: false });
            } else {
                self.setState({ hasServerError: true, preparingOptions: false });
            }
        });
    }
    toggleSlscore () {
        this.setState({ showSlscorePop: true });
    }
    dismissSlscore () {
        this.setState({ showSlscorePop: false });
    }
    onSubmit (data) {
        const typeId = data.type;
        const levelId = data.work_year;
        const answer = data.answer;
        const langCode = data.language;
        const postData = {};
        postData.exp_level = levelId;
        postData.job_type = typeId;
        postData.open_text = answer;
        postData.lang_code = langCode;
        this.postDataToServer(postData);
    }
    postDataToServer (data) {
        this.setState({ isLoading: true, isfetchingData: true, hasServerError: false });
        const csrftoken = Cookie.get('csrftoken');
        const self = this;
        const code = this.state.invitationCode;
        Request
        .post(self.props.postDemoData)
        .set('X-CSRFToken', csrftoken)
        .set('X-Invite-Code', code)
        .send(data)
        .end((err, res) => {
            if (err || !res.ok) {
                self.setState({ isLoading: false });
                self.setState({ hasServerError: true });
            } else if (res.status === 201) {
                const responseJson = JSON.parse(res.text);
                const applicationID = responseJson.id;
                self.setState({ applicationID });
                self.getDemoResult(self.state.applicationID);
            } else if (res.status === 403) {
                self.setState({
                    codePass: false,
                    codeAuthFail: true,
                });
            } else {
                self.setState({ hasServerError: true });
            }
        });
    }
    getDemoResult (applicationID) {
        const url = this.props.getDemoResult + applicationID;
        const self = this;
        Request
            .get(url)
            .end((err, res) => {
                if (err || !res.ok) {
                    self.setState({ isLoading: false });
                    self.setState({ hasServerError: true });
                } else {
                    if (res.status === 202) {
                        setTimeout(() => { self.getDemoResult(self.state.applicationID); }, 5000);
                    }
                    if (res.status === 200) {
                        const responseJson = JSON.parse(res.text);
                        let seedlinkScore = responseJson.seedlink_score;
                        seedlinkScore = seedlinkScore < 0 ? 0 : seedlinkScore;
                        seedlinkScore = seedlinkScore > 100 ? 100 : seedlinkScore;
                        const competencys = responseJson.competency_scores;
                        for (let i = 0; i < competencys.length; i++) {
                            competencys[i].range_max = parseInt(competencys[i].range_max);
                            competencys[i].range_min = parseInt(competencys[i].range_min);
                            competencys[i].score = parseInt(competencys[i].score);
                            competencys[i].range_max = competencys[i].range_max < 0 ? 0 : competencys[i].range_max;
                            competencys[i].range_max = competencys[i].range_max > 100 ? 100 : competencys[i].range_max;
                            competencys[i].range_min = competencys[i].range_min < 0 ? 0 : competencys[i].range_min;
                            competencys[i].range_min = competencys[i].range_min > 100 ? 100 : competencys[i].range_min;
                            competencys[i].score = competencys[i].score < 0 ? 0 : competencys[i].score;
                            competencys[i].score = competencys[i].score > 100 ? 100 : competencys[i].score;
                        }
                        self.setState(
                            { competencys,
                                slscore: parseInt(seedlinkScore.score),
                                slscoreBrief: seedlinkScore.brief,
                                competencyDes: responseJson.competency_desc,
                            },
                        );
                        self.setState({ isLoading: false, isfetchingData: false });
                        const resultPanel = ReactDOM.findDOMNode(self.refs.resultContainer);
                        resultPanel.scrollIntoView();
                    }
                }
            });
    }
    shouldSubmit (e) {
        if (e.keyCode === 13) {
            this.checkCode();
        }
    }
    updateCode (e) {
        this.setState({
            invitationCode: e.target.value,
            codeAuthFail: false,
        });
    }
    checkCode () {
        this.setState({
            startCheckingCode: true,
        });
        const csrftoken = Cookie.get('csrftoken');
        const self = this;
        const code = this.state.invitationCode;
        if (code !== '') {
            self.setState({
                isCheckingCode: true,
            });
            Request
            .post('/api/v2/demo/auth/')
            .set('X-CSRFToken', csrftoken)
            .set('X-Invite-Code', code)
            .end((err, res) => {
                if (err || !res.ok) {
                    self.setState({
                        codeAuthFail: true,
                        isCheckingCode: false,
                    });
                } else if (res.status === 200) {
                    self.setState({
                        codePass: true,
                        codeAuthFail: false,
                        isCheckingCode: false,
                    });
                } else if (res.status === 403) {
                    self.setState({
                        codeAuthFail: true,
                        isCheckingCode: false,
                    });
                } else {
                    self.setState({
                        codeAuthFail: true,
                        isCheckingCode: false,
                    });
                }
            });
        } else {
            // self.setState({
            //     codeIsEmpty: true,
            // });
        }
    }
    render () {
        // const isEmptyCode = this.state.startCheckingCode && this.state.codeIsEmpty;
        return (
            <div className="wrapper">
                <HeaderBoard />
                { !this.state.codePass ?
                    <div className="invitationBanner">
                        <div className="background-img">
                            <div className="invi-wrapper">
                                <FormattedMessage id="hint_input_invitation_code" tagName="p" />
                                <input onKeyDown={this.shouldSubmit} value={this.state.invitationCode} onChange={this.updateCode} />
                                { this.state.startCheckingCode && this.state.invitationCode === '' ?
                                    <FormattedMessage id="hint_input_invitation_code" /> : null
                                }
                                { this.state.codeAuthFail ?
                                    <FormattedMessage id="notify.invalid_invitation_code" /> : null
                                }
                                <button disabled={this.state.isCheckingCode} onClick={this.checkCode}><FormattedMessage id="button.submit" tagName="i" /></button>
                            </div>
                        </div>
                    </div>

                    :
                    <div className="loading-wrapper">
                        {this.state.isLoading ? <div className="demo-loading-cover"><div className="loading-animation"><p>Analyzing...</p><i /><i /><i /><i /><i /><i /></div></div> : null}
                        <HeaderBanner />
                        <div className="demo-wrapper">
                            { !this.state.preparingOptions ?
                                <DemoSubmit jobTypes={this.state.jobTypes} expLevels={this.state.expLevels} callbackParent={this.onSubmit} />
                                :
                                <p className="init-message"><FormattedMessage id="stat_loading" /></p>
                            }
                            {this.state.hasServerError ? <p className="serverError"><FormattedMessage id="error.loading_failed" /></p> : null}
                            { !this.state.isfetchingData ?
                                <div ref="resultContainer" className="demo-result-wrapper">
                                    <div className="demo-result hr-main">
                                        <div className="slscore-wrapper">
                                            <div className="slscore">
                                                <div className="title">
                                                    <FormattedMessage id="seedlinkscore.0" />
                                                    <span className="slscore-version"><FormattedMessage id="seedlinkscore.pro" /></span>
                                                    <div className="help-icon" onMouseOver={this.toggleSlscore} onMouseLeave={this.dismissSlscore}>
                                                        {this.state.showSlscorePop ? <SlscorePop /> : null}
                                                    </div>
                                                </div>
                                                <div className="chartWrapper">
                                                    { this.state.slscore === 0 ?
                                                        <p className="zeroScore">{this.state.slscore}</p>
                                                        :
                                                        <DonutPie myScore={this.state.slscore} />
                                                    }
                                                </div>
                                                <p className="good">
                                                    <FormattedMessage id={this.state.slscoreBrief} />
                                                </p>
                                            </div>
                                            <div />
                                            <div className="description">
                                                <p className="des-title"><FormattedMessage id="demo.competency_description" /></p>
                                                <hr />
                                                <div className="des-content">
                                                    {this.state.competencyDes.map((value, index) => (
                                                        <p key={index}><FormattedMessage id={value} /></p>
                                                ))}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="result-bar-wrapper">
                                            <div className="bar-legend">
                                                <FormattedMessage id="hr.competency" tagName="p" />
                                                <div className="legend-container">
                                                    <div className="score">
                                                        <div />
                                                        <FormattedMessage id="demo.my_score" />
                                                    </div>
                                                    <div className="range">
                                                        <div />
                                                        <FormattedMessage id="seedlinkscore.expected_range" />
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>
                                            {this.state.competencys.map((value, index) => (
                                                <DemoResultBar key={index} myScore={value.score} startScore={value.range_min} endScore={value.range_max} scoreProperty={value.name} />
                                        ))}
                                        </div>
                                    </div>
                                </div> : null
                        }
                        </div>
                    </div>
                }

                <DemoFooter />
            </div>
        );
    }
}
Demo.propTypes = {
    getOptions: React.PropTypes.string,
    postDemoData: React.PropTypes.string,
    getDemoResult: React.PropTypes.string,
};
Demo.defaultProps = {
    getOptions: '/api/v2/configs/demo-choices/',
    postDemoData: '/api/v2/demo/application/',
    getDemoResult: '/api/v2/demo/application/',
};

export default Demo;
