import React from 'react';
// import './base.scss';
import { IntlProvider } from 'react-intl';
import 'intl';


class App extends React.Component {

    getLanguage () {
        const language = navigator.languages ? navigator.languages[0] : navigator.language || navigator.userLanguage;
        const generalLang = language.substring(0, 2);
        let useLang;
        if (generalLang === 'zh') {
            useLang = 'zh';
        } else {
            useLang = 'en';
        }
        return useLang;
    }
    flattenMessages (nestedMessages, prefix = '') {
        const self = this;
        return Object.keys(nestedMessages).reduce((msg, key) => {
            const messages = { ...msg };
            const value = nestedMessages[key];
            const prefixedKey = prefix ? `${prefix}.${key}` : key;

            if (typeof value === 'string') {
                messages[prefixedKey] = value;
            } else {
                Object.assign(messages, self.flattenMessages(value, prefixedKey));
            }

            return messages;
        }, {});
    }
    chooseLocale () {
        let zh = require('../../../../locales/zh-CN/rcxue-json-zh-CN.json');
        let en = require('../../../../locales/en/rcxue-json-en.json');
        let nl = require('../../../../locales/nl-NL/rcxue-json-nl-NL.json');

        zh = this.flattenMessages(zh);
        en = this.flattenMessages(en);
        nl = this.flattenMessages(nl);

        switch (this.getLanguage()) {
            case 'en':
                return en;
                break;
            case 'zh':
                return zh;
                break;
            case 'nl':
                return nl;
                break;
            default:
                return en;
                break;
        }
    }


    render () {
        return (
            <IntlProvider locale="en" messages={this.chooseLocale()} >
                <div>
                    {this.props.children}
                </div>
            </IntlProvider>
        );
    }
}


export default App;
