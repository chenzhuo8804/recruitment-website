import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import { Login } from '../components/home';


const propTypes = {
    // children: PropTypes.element,
    className: PropTypes.string,
};

class Home extends Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { className } = this.props;

        return (
            <div className={classNames('home', className)}>
                <Login />
            </div>
        );
    }
}

Home.propTypes = propTypes;

export default Home;
