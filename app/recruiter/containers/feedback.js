import React from 'react';
// import './index.scss';
import request from 'superagent';
import { FormattedMessage } from 'react-intl'; // <FormattedMessage id="seedlinkScore"/>
import { JobseekerHeader, ErrorMessage, FileUpload } from '../components/feedback';
import List from '../components/list';
import Cookie from '../../helpers/cookie';

class Feedback extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            imgUploading: false,
            startValidation: false,
            submitSuccess: false,
            hasScreenshot: false,
            isServerError: false,
            isPostingData: false,
            position: '',
            positionID: '',
            hiddenPosition: '',
            name: '',
            contact: '',
            problem: '',
            description: '',
            fileKey: '',
            faqType: [],
        };
        this.updateInputName = this.updateInputName.bind(this);
        this.updateInputPosition = this.updateInputPosition.bind(this);
        this.updateHiddenPosition = this.updateHiddenPosition.bind(this);
        this.updateInputContact = this.updateInputContact.bind(this);
        this.getItem = this.getItem.bind(this);
        this.updateInputDescription = this.updateInputDescription.bind(this);
        this.getFileKey = this.getFileKey.bind(this);
        this.submitFeedback = this.submitFeedback.bind(this);
    }
    componentDidMount () {
        const self = this;
        request
        .get(self.props.getFeedbackData)
        // .set('Authorization', 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFteS5zaGFuZ0BzZWVkbGlua3RlY2guY29tIiwidXNlcl9pZCI6NzU3MTksImVtYWlsIjoiYW15LnNoYW5nQHNlZWRsaW5rdGVjaC5jb20iLCJleHAiOjE0ODA0MDI1NTV9.Zh-d7x2RBXwJ757jVi-JSkDvlJlhHkPeIcJ1mzmAz-k')
        .end((err, res) => {
            if (err || !res.ok) {
            } else if (res.status === 200) {
                const responseJson = JSON.parse(res.text);
                self.processData(responseJson);
            } else {
            }
        });
    }
    processData (data) {
        let {
            real_name: userName,
            position_name: positionName,
            position_id: positionID,
            contact: contactInfo,
        } = data;

        this.setState({ faqType: data.faq_type, positionID });
        this.setState({ name: userName, position: positionName, contact: contactInfo });
    }
    getItem (item) {
        this.setState({
            problem: item,
        });
    }
    getFileKey (key) {
        if (key) {
            this.setState({ hasScreenshot: true });
            this.setState({ fileKey: key });
        }
    }
    validateInput () {
        const name = this.state.name;
        const position = this.state.position;
        const positionID = this.state.positionID;
        const contact = this.state.contact;
        const problem = this.state.problem;
        const description = this.state.description;
        const fileKey = this.state.fileKey;
        if (name !== '' && position !== '' && contact !== '' && problem !== '' && description !== '') {
            let screenshotFilekey = '';
            if (this.state.hasScreenshot) {
                screenshotFilekey = fileKey;
            }

            const dataToPost = {
                name,
                position_id: positionID,
                position_name: position,
                contact,
                faq_type: problem,
                description,
                screenshot_filekey: screenshotFilekey,
            };

            this.postDataToServer(dataToPost);
        } else {

        }
    }
    submitFeedback (e) {
        this.setState({ startValidation: true });
        this.validateInput();
        e.preventDefault();
    }
    postDataToServer (data) {
        this.setState({ isPostingData: true });
        const self = this;
        const csrftoken = Cookie.get('csrftoken');
        request
        .post(self.props.postFeedbackData)
        .send(data)
        .set('X-CSRFToken', csrftoken)
        // .set('Authorization', 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFteS5zaGFuZ0BzZWVkbGlua3RlY2guY29tIiwidXNlcl9pZCI6NzU3MTksImVtYWlsIjoiYW15LnNoYW5nQHNlZWRsaW5rdGVjaC5jb20iLCJleHAiOjE0ODA0MDI2MDF9.AvTHJz9T-5XqdMN-Uh4RwfDZfTU60K3mIvVzgPuaGPk')
        .end((err, res) => {
            if (err || !res.ok) {
                self.setState({ isServerError: true, isPostingData: false });
            } else if (res.status === 201) {
                self.setState({ submitSuccess: true, isPostingData: false });
            } else {
                self.setState({ isServerError: true, isPostingData: false });
            }
        });
    }
    updateInputName (evt) {
        this.setState({
            name: evt.target.value,
        });
    }
    updateInputPosition (evt) {
        this.setState({
            position: evt.target.value,
        });
    }
    updateHiddenPosition (evt) {
        this.setState({
            positionID: evt.target.value,
        });
    }
    updateInputContact (evt) {
        this.setState({
            contact: evt.target.value,
        });
    }
    updateInputDescription (evt) {
        this.setState({
            description: evt.target.value,
        });
    }

    render () {
        const imageType = 'image/bmp,image/gif,image/jpg,image/jpeg,image/png,image/tif,image/tiff';
        return (
            <div className="feedback-wrapper">
                <div className="base-wrapper">
                    <div className="main">
                        <JobseekerHeader />
                        { !this.state.submitSuccess ?
                            <div className="strike">
                                <span><FormattedMessage id="js.feedback.title" /></span>
                            </div> : null
                        }
                        { this.state.submitSuccess ?
                            <div className="feedbackThankyou">
                                <p><FormattedMessage id="js.feedback.problem_thankyou" /></p>
                                <a href="/feedback/"><FormattedMessage id="js.feedback.problem_return" /></a>
                            </div>
                            :
                            <div className="feedback-body-wrapper">
                                <div className="feedbackInput">
                                    <p><FormattedMessage id="js.feedback.name" /></p>
                                    <input ref="nameInput" value={this.state.name} onChange={this.updateInputName} />
                                    {this.state.startValidation && this.state.name === '' ? <ErrorMessage errorContent="hint_input_name" /> : null}
                                </div>
                                <div className="feedbackInput">
                                    <p><FormattedMessage id="js.feedback.position" /></p>
                                    <input placeholder="Input company name and position name" ref="positionInput" value={this.state.position} onChange={this.updateInputPosition} />
                                    {this.state.startValidation && this.state.position === '' ? <ErrorMessage errorContent="hint_input_position" /> : null}
                                    <input ref="positionHidden" type="hidden" value={this.state.positionID} onChange={this.updateHiddenPosition} />
                                </div>
                                <div className="feedbackInput">
                                    <p><FormattedMessage id="js.feedback.email_mobile" /></p>
                                    <input ref="contactInput" value={this.state.contact} onChange={this.updateInputContact} />
                                    {this.state.startValidation && this.state.contact === '' ? <ErrorMessage errorContent="hint_input_contact" /> : null}
                                </div>
                                <div className="problems">
                                    <p><FormattedMessage id="hint_select_problem" /></p>
                                    <List data={this.state.faqType} getSelectedData={this.getItem} />
                                    {this.state.startValidation && this.state.problem === '' ? <ErrorMessage errorContent="hint_select_problem" /> : null}
                                </div>
                                <div className="problem-des">
                                    <p><FormattedMessage id="js.feedback.problem_description" /></p>
                                    <textarea value={this.state.description} onChange={this.updateInputDescription} />
                                    {this.state.startValidation && this.state.description === '' ? <ErrorMessage errorContent="hint_input_problem_description" /> : null}
                                </div>
                                <div className="file-upload">
                                    <p><FormattedMessage id="js.feedback.problem_upload_screenshot" /></p>
                                    <FileUpload acceptFileType={imageType} getTokenUrl={this.props.imageAPI} sendFileKey={this.getFileKey} />
                                </div>
                                { this.state.isServerError ?
                                    <p className="serverError"><FormattedMessage id="error.submit_failed" /></p> : null
                            }
                            </div>
                    }
                    </div>
                </div>

                { !this.state.submitSuccess ?
                    <div className="feedback-footer">
                        <button disabled={this.state.isPostingData} className="submit" onClick={this.submitFeedback}><FormattedMessage id="button.submit" /></button>
                    </div> : null
            }
            </div>

        );
    }
}
Feedback.propTypes = {
    imageAPI: React.PropTypes.string,
    getFeedbackData: React.PropTypes.string,
};
Feedback.defaultProps = {
    imageAPI: '/api/v2/upload/obtain-image-token',
    getFeedbackData: '/api/v2/feedback/foretell/',
    postFeedbackData: '/api/v2/feedback/',
};
export default Feedback;
