import React from 'react';
import ReactDOM from 'react-dom';
import Request from 'superagent';
import { FormattedMessage } from 'react-intl';
// import './index.scss';
import { SmallBanner } from '../components/companyInfo';
import List from '../components/list';
import Cookie from '../../helpers/cookie';

// http://127.0.0.1:8080/api/v2/drago/company/3/quota/

class CompanyInfo extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            startValidation: false,
            showContactPop: false,
            showDropdownList: false,
            shouldDoPost: false,
            shouldDoPut: false,
            savedSuccesslly: false,
            isSavingData: false,
            expired: false,
            expireTime: '',
            leftDays: null,
            candiateCount: null,
            candiateRemain: null,
            companyName: '',
            logoImage: '',
            logoUrl: '',
            industry: '',
            industryID: '',
            location: '',
            domain: '',
            companyIntro: '',
            selectedIndustry: '',
            industryList: [],
            companyID: '',
        };
        this.dismissContactPop = this.dismissContactPop.bind(this);
        this.showDropdownList = this.showDropdownList.bind(this);
        this.showContactPop = this.showContactPop.bind(this);
        this.updateCompanyName = this.updateCompanyName.bind(this);
        this.onClick = this.onClick.bind(this);
        this.showDropdownList = this.showDropdownList.bind(this);
        this.getOptions = this.getOptions.bind(this);
        this.updateLocation = this.updateLocation.bind(this);
        this.updateDomain = this.updateDomain.bind(this);
        this.updateCompanyDes = this.updateCompanyDes.bind(this);
        this.saveAccountInfo = this.saveAccountInfo.bind(this);
    }
    componentDidMount () {
        const csrftoken = Cookie.get('csrftoken');
        const self = this;
        Request
        .get('/api/v2/configs/industry/')
        .end((err, res) => {
            if (err || !res.ok) {
            } else if (res.status === 200) {
                const responseJson = JSON.parse(res.text);
                self.setState({
                    industryList: responseJson,
                });
            } else {
            }
        });
        Request
        .get('/api/v2/company/')
        // .set('X-CSRFToken', csrftoken)
        .end((err, res) => {
            if (err || !res.ok) {
            } else if (res.status === 200) {
                let company = JSON.parse(res.text);
                company = company[0];
                if (company === undefined) {
                    self.setState({
                        shouldDoPost: true,
                        shouldDoPut: false,
                    });
                } else {
                    self.setState({
                        shouldDoPost: false,
                        shouldDoPut: true,
                    });
                    self.setState(
                        {
                            companyName: company.name,
                            logoUrl: company.logo,
                            industry: company.industry.name,
                            industryID: company.industry.id,
                            location: company.address,
                            domain: company.email_domain,
                            companyIntro: company.description,
                            companyID: company.id,
                        },
                    );
                }
                self.getCompanyQuota(self.state.companyID);
            }
        });
    }
    getCompanyQuota (company) {
        const self = this;
        Request
        .get('/api/v2/user/me/')
        .end((err, res) => {
            if (err || !res.ok) {
            } else if (res.status === 200) {
                const companyQuota = JSON.parse(res.text);
                const initialDate = companyQuota.account_expire;
                const remainingNum = companyQuota.screen_balance;
                const totalNum = companyQuota.screen_total;
                self.setState({
                    candiateCount: totalNum,
                    candiateRemain: remainingNum,
                });
                if (initialDate === null) {
                    self.setState({
                        expired: true,
                    });
                } else {
                    self.setState({
                        expired: false,
                    });
                    const dates = initialDate.split('-');
                    const expireDate = `${dates[0]}/${dates[1]}/${dates[2]}`;
                    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                    const currentTime = new Date();
                    const month = currentTime.getMonth() + 1;
                    const day = currentTime.getDate();
                    const year = currentTime.getFullYear();
                    const firstDate = new Date(year, month, day);
                    const secondDate = new Date(dates[0], dates[1], dates[2]);
                    const diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                    self.setState({
                        expireTime: expireDate,
                        leftDays: diffDays,
                    });
                }
            }
        });
    }
    imageInput (event) {
        const logoFile = event.target.files;
        const imageContainer = ReactDOM.findDOMNode(this.refs.logoBed);
        const tempLogoUrl = URL.createObjectURL(event.target.files[0]);
        this.setState({
            logoImage: logoFile[0],
            logoUrl: tempLogoUrl,
        });
    }
    onClick () {
        this.open();
    }

    open () {
        const fileInput = ReactDOM.findDOMNode(this.refs.fileInput);
        fileInput.value = null;
        fileInput.click();
    }
    updateCompanyName (e) {
        this.setState({
            companyName: e.target.value,
        });
    }
    updateLocation (e) {
        this.setState({
            location: e.target.value,
        });
    }
    updateDomain (e) {
        this.setState({
            domain: e.target.value,
        });
    }
    updateCompanyDes (e) {
        this.setState({
            companyIntro: e.target.value,
        });
    }
    saveAccountInfo (e) {
        this.setState({ startValidation: true });
        this.validateInput();
        e.preventDefault();
    }
    validateInput () {
        const name = this.state.companyName;
        const logoUrl = this.state.logoUrl;
        const logoImage = this.state.logoImage;
        // let industry = this.state.industry;
        const industryID = this.state.industryID;
        const location = this.state.location;
        const domain = this.state.domain;
        const description = this.state.companyIntro;
        const validResult = name !== '' && logoUrl !== '' && industryID !== '' && location !== '' && domain !== '' && description !== '';
        if (validResult) {
            const postData = new FormData();
            postData.append('name', name);
            if (this.state.logoImage !== '') {
                postData.append('logo', logoImage);
            }
            postData.append('industry_id', industryID);
            postData.append('address', location);
            postData.append('email_domain', domain);
            postData.append('description', description);
            this.postDataToServer(postData);
        }
    }
    postDataToServer (data) {
        const csrftoken = Cookie.get('csrftoken');
        const self = this;
        self.setState({
            isSavingData: true,
        });
        if (self.state.shouldDoPost) {
            Request
            .post('/api/v2/company/')
            .set('X-CSRFToken', csrftoken)
            .send(data)
            .end((err, res) => {
                if (err || !res.ok) {
                } else if (res.status === 200) {
                    self.setState({
                        savedSuccesslly: true,
                        isSavingData: false,
                    });
                    setTimeout(() => {
                        self.setState({
                            savedSuccesslly: false,
                        });
                    }, 1000);
                } else {
                    self.setState({
                        savedSuccesslly: false,
                    });
                }
            });
        }
        if (self.state.shouldDoPut) {
            Request
            .put(`/api/v2/company/${self.state.companyID}/`)
            .set('X-CSRFToken', csrftoken)
            .send(data)
            .end((err, res) => {
                if (err || !res.ok) {
                } else if (res.status === 201) {
                    self.setState({
                        savedSuccesslly: true,
                        isSavingData: false,
                    });
                    setTimeout(() => {
                        self.setState({
                            savedSuccesslly: false,
                        });
                    }, 1000);
                } else {

                }
            });
        }
    }
    showContactPop () {
        this.setState({
            showContactPop: true,
        });
    }
    dismissContactPop () {
        this.setState({
            showContactPop: false,
        });
    }
    showDropdownList () {
        this.setState({
            showDropdownList: true,
        });
    }
    dismissDropdownList () {
        this.setState({
            showDropdownList: false,
        });
    }
    getOptions (optionID, optionName) {
        this.setState({
            showDropdownList: false,
            industryID: optionID,
            industry: optionName,
        });
    }


    render () {
        return (
            <div className="info-wrapper">
                { this.state.showContactPop ?
                    <div className="loading-wrapper" /> : null
                }
                { this.state.showContactPop ?
                    <div className="contact-pop">
                        <p className="contact-title"><FormattedMessage id="button.contact_us" /></p>
                        <div className="contact-body">
                            <p className="contact-tel"><FormattedMessage id="hr.contact_us_tel" />: (86 21) 605 28 208</p>
                            <p className="contact-email"><FormattedMessage id="hr.contact_us_email" />: info@seedlinktech.com</p>
                        </div>
                        <div className="contact-ok" onClick={this.dismissContactPop}>
                            <div><FormattedMessage id="button.ok" /></div>
                        </div>
                        <div className="contact-close" onClick={this.dismissContactPop}>
                            <div className="iconfont icon-delete" />
                        </div>
                    </div> : null
                }

                <div className="paper-container">
                    <div className="paper">
                        <div className="pane">
                            <SmallBanner content="hr.company.accounts" />
                            <div className="item-row first-item-row">
                                <span className="label account"><FormattedMessage id="hr.company.expire_date" />:</span>
                                { this.state.expired ?
                                    <div className="right-side expireInfo"><FormattedMessage id="hr.company_expired" /></div>
                                    :
                                    <div className="right-side">
                                        {this.state.expireTime}&nbsp;
                                        (<FormattedMessage
                                            id="hr.company.expire_remaining"
                                            tagName="span"
                                            values={{
                                                day: <span className="highlight-word">{this.state.leftDays}</span>,
                                                s: this.state.leftDays > 1 ? 's' : '',
                                            }}
                                        />)
                                    </div>
                                }

                            </div>
                            <div className="item-row contact-row">
                                <button className="contact-button" onClick={this.showContactPop}><FormattedMessage id="button.contact_us" /></button>
                            </div>
                            <div className="item-row row last-row">
                                <span className="label account"><FormattedMessage id="hr.company.assessment_count" />:</span>
                                <div className="right-side">
                                    {this.state.candiateCount}&nbsp;
                                (<FormattedMessage
                                    id="hr.company.assessment_remaining"
                                    tagName="span"
                                    values={{
                                        candidate: <span className="highlight-word">{this.state.candiateRemain}</span>,
                                        s: this.state.candiateRemain > 1 ? 's' : '',
                                    }}
                                />)
                                </div>
                            </div>
                            <SmallBanner content="hr.company.profile" />
                            <div className="item-row row first-item-row">
                                <span className="label company"><FormattedMessage id="hr.company.name" />:</span>
                                <input value={this.state.companyName} onChange={this.updateCompanyName} className="right-side input" />
                                {this.state.startValidation && this.state.companyName === '' ?
                                    <p className="right-side highlight-error">
                                        <FormattedMessage id="hint_input_required_field" /></p>
                                    : null
                                }
                            </div>
                            <div className="item-row logo-row last-row">
                                <span className="label company"><FormattedMessage id="hr.company.logo" />:</span>
                                <div className="right-side">
                                    <div className="logo-wrapper">
                                        <img src={this.state.logoUrl} ref="logoBed" />
                                        <input ref="fileInput" className="image-input" type="file" accept="image/bmp,image/gif,image/jpg,image/jpeg,image/png,image/tif,image/tiff" onChange={this.imageInput.bind(this)} />
                                        <div className="imageHint">
                                            <p><FormattedMessage id="hint_company_logo_size" /></p>
                                        </div>
                                    </div>
                                    <button className="upload-image" onClick={this.onClick}><FormattedMessage id="button.upload" /></button>
                                </div>
                                {this.state.startValidation && this.state.logoUrl === '' ?
                                    <p className="right-side highlight-error company-logo-error"><FormattedMessage id="hint_input_required_field" /></p> : null
                                }
                            </div>
                            <div className="item-row row last-row">
                                <span className="label company"><FormattedMessage id="hr.company.industry" />:</span>
                                <div className="right-side dropdown-div" onMouseEnter={this.showDropdownList} onMouseLeave={this.dismissDropdownList.bind(this)}>
                                    { this.state.industry !== '' ?
                                        <FormattedMessage id={this.state.industry} /> : ''
                                    }
                                    <div className="arrow iconfont icon-fold" />
                                    { this.state.showDropdownList ?
                                        <div className="options">
                                            <List getSelectedData={this.getOptions} data={this.state.industryList} />
                                        </div> : null
                                    }

                                </div>
                                {this.state.startValidation && this.state.industry === '' ?
                                    <p className="right-side highlight-error"><FormattedMessage id="hint_input_required_field" /></p> : null
                                }
                            </div>
                            <div className="item-row row last-row">
                                <span className="label company"><FormattedMessage id="hr.company.address" />:</span>
                                <input value={this.state.location} onChange={this.updateLocation} className="right-side input" />
                                {this.state.startValidation && this.state.location === '' ?
                                    <p className="right-side highlight-error"><FormattedMessage id="hint_input_required_field" /></p> : null
                                }
                            </div>
                            <div className="item-row last-row domain-row">
                                <span className="label company"><FormattedMessage id="hr.company.domain" />:</span>
                                <input placeholder="E.g. rcxue.com" value={this.state.domain} onChange={this.updateDomain} className="right-side input" />
                                <p className="right-side highlight-word hint">*Employees have to use the same company domain to rate when building company model in People Insights</p>
                                {this.state.startValidation && this.state.domain === '' ?
                                    <p className="right-side highlight-error"><FormattedMessage id="hint_input_required_field" /></p> : null
                                }
                            </div>
                            <div className="item-row textarea-row">
                                <span className="label company"><FormattedMessage id="hr.company.intro" />:</span>
                                <textarea value={this.state.companyIntro} onChange={this.updateCompanyDes} className="right-side input" />
                                {this.state.startValidation && this.state.companyIntro === '' ?
                                    <p className="right-side highlight-error"><FormattedMessage id="hint_input_required_field" /></p> : null

                                }
                                <div className="button-row right-side">
                                    <button disabled={this.state.isSavingData} className="saveInfoButton" onClick={this.saveAccountInfo}><FormattedMessage id="button.save" />
                                        {this.state.savedSuccesslly ?
                                            <span className="tooltiptext"><FormattedMessage id="notify.save_successfully" /></span>
                                            :
                                            null
                                        }
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CompanyInfo;
