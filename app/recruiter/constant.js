export default {
    contactPhone: '+86-21-6111-6264',
    contactMail: 'info@seedlinktech.com',
    copyright: '© 2014 - 2016 Seedlink Technology',
    homeSite: 'http://seedlinktech.com/',
    icpCode: '沪ICP备13034759号-2',
};
