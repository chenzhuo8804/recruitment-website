import React from 'react';
import { Route, IndexRoute } from 'react-router';

import { Cookie } from '../helpers';
import { App, Home, NotFound, CompanyInfo, Demo, Feedback, Rating } from './containers';

const { Landing, Admin } = App.layoutTypes;

const requireLogin = (nextState, replace, cb) => {
    const sessionKey = Cookie.get('sessionid');
    const token = Cookie.get('token');

    if (!sessionKey && !token) {
        // replace('/');
    }
    cb();
};

/**
 * Please keep routes in alphabetical order
 */
export default (
    <Route path="/">
        {/* using landing layout */}
        <Route component={App} layout={Landing}>
            <IndexRoute component={Home} />
            {/* requiring login */}
            <Route onEnter={requireLogin} />
        </Route>

        {/* using admin layout, requiring login */}
        <Route component={App} onEnter={requireLogin} layout={Admin}>
            <Route path="companyinfo" component={CompanyInfo} />
        </Route>
        <Route path="rating/:pid/:viewingUid/:candidateName/:myRank/:allRank/:tabIndex/:mid/:page(/:shareToken)" component={Rating} />
        <Route path="demo" component={Demo} />
        <Route path="otherfeedbacks" component={Feedback} />
        {/* catch not found */}
        {/* <Route path="*" component={NotFound} status={404} /> */}
    </Route>
);
