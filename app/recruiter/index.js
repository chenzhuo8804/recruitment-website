import 'babel-polyfill';
import 'intl';

import React from 'react';
import { render } from 'react-dom';
import { IntlProvider } from 'react-intl';
import { Router, browserHistory } from 'react-router';

import { Locales } from '../helpers';
import routes from './routes';
import './styles/main.scss';


render(
    <IntlProvider locale={Locales.getLocale()} messages={Locales.getMessages()}>
        <Router routes={routes} history={browserHistory} />
    </IntlProvider>,
    document.querySelector('#root'),
);

window.React = React;
