
## Principle

- Only one source of truth
- Don't repeat yourself ([DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself))

## How We Make Components ([Thinking in react](https://facebook.github.io/react/docs/thinking-in-react.html))

- Break the UI into a component hierarchy with [Single responsibility principle](https://en.wikipedia.org/wiki/Single_responsibility_principle)

- Build a static version in React

- Identify the minimal (but complete) representation of UI state

> - Is it passed in from a parent via props? If so, it probably isn't state.
> - Does it remain unchanged over time? If so, it probably isn't state.
> - Can you compute it based on any other state or props in your component? If so, it isn't state.

- Identify where your state should live

> - Identify every component that renders something based on that state.
> - Find a common owner component (a single component above all the components that need the state in the hierarchy).
> - Either the common owner or another component higher up in the hierarchy should own the state.
> - If you can't find a component where it makes sense to own the state, create a new component simply for holding the state and add it somewhere in the hierarchy above the common owner component.

- Add inverse data flow

## Installation

Assumed you have git cloned this repository to your local and have [Node.js](https://nodejs.org/en/) installed
```bash
$ npm install
```

It will be better if you also install some global package
```bash
$ sudo npm install -g eslint webpack webpack-dev-server
```

Then enjoy your development with
```bash
$ npm run dev
```

## Usage

- init npm packages
```bash
$ npm install
```

- run development server with local server api
```bash
$ npm run dev-local
```
or
```bash
$ npm run dev
```

- run development server with test server api, will enable `https://` protocol
```bash
$ npm run dev-test
```


- clean installed node modules when package.json's dependencies changed
```bash
$ npm run clean-install
```

## Code Style Reference

- [ES6](https://github.com/airbnb/javascript)
- [JSX](https://github.com/airbnb/javascript/tree/master/react)
- [CSS & SASS](https://github.com/airbnb/css)
- [CSS in Javascript](https://github.com/airbnb/javascript/tree/master/css-in-javascript)
