var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var ExtractTextPlugin = require('extract-text-webpack-plugin');


PORT = 6060;
API_TARGET = 'http://127.0.0.1:8080'
PUBLIC_ROOT = '/static/media/dist/'

var extractCSS = new ExtractTextPlugin('site.[hash].css');


module.exports = {
    entry: [
        'webpack-dev-server/client?http://localhost:' + PORT,
        'webpack/hot/only-dev-server',
        './src/index.js',
    ],
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel',
            },
            {
                test: /\.scss$/,
                loader: extractCSS.extract('style-loader', 'css!postcss!sass'),
            },
            {
                test: /\.css$/,
                loader: extractCSS.extract('style-loader', 'css'),
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                loader: 'url?limit=8192&name=./imgs/[name].[ext]',
            },
            {
                test: /\.(woff|woff2|ttf|eot|svg)(\?t=[0-9]+)?$/,
                loader: 'url?limit=8192&name=./imgs/[name].[ext]',
            },
            {
                test: /\.json$/,
                loader: 'json'
            },

        ]
    },
    resolve: {
        extensions: ['', '.js'],
    },
    output: {
        path: 'dist',
        // publicPath: PUBLIC_ROOT,
        filename: 'bundle.[hash].js',
    },
    devServer: {
        contentBase: './dist',
        host: '0.0.0.0',
        port: PORT,
        https: true,
        historyApiFallback: true,
        // publicPath: PUBLIC_ROOT,
        proxy: {
            '/api': {
                target: API_TARGET,
                secure: false,
            },
            '/open': {
                target: API_TARGET,
                secure: false,
            },
            '/admin': {
                target: API_TARGET,
                secure: false,
            },
            '/static/media/grappelli/': {
                target: API_TARGET,
                secure: false,
            },
        },
        hot: true,
    },
    plugins: [
        extractCSS,
        new HtmlWebpackPlugin({
            // Required by html-webpack-template
            inject: false,
            template: require('html-webpack-template'),
            //template: 'node_modules/html-webpack-template/index.ejs',

            // template options
            appMountId: 'app',

            // html webpack options
            title: 'Seedlink AI',
        }),
        new webpack.HotModuleReplacementPlugin(),
    ]
};
